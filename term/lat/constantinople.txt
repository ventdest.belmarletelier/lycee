CONSTANTINOPLE
==============

1. Byzance
----------

a. Colonie grecque
Après fondation en -667 par Mégare, Byzance est une cité qui devient vite prospère grâce au contrôle du commerce de la Mer Noire, facilité par sa position géographique stratégique : Elle est située au bord d'un détroit et entourée de sept collines qui en font une cité très facile à défendre.    

b. Mythe de la fondation 
NOAH

c. Conquête par Rome
Conquise avec toute la Grèce au premier siècle.
Siège par Septime Sévère dans la continuité de la guerre civile des années 190 => Byzance se rend aux Romains et est soumise à Périnthe, cité rivale, en guise de punition.
À la demande de Caracalla, la ville est tout de même reconstruite. Elle recouvre son rôle de pôle de l'hellenisme dans la région.

d. Ruines
NOAH 


2. Constantinople
-----------------

a. Fondation de Constantinople

Objectif:
Améliorer la défense de l'Orient suite à la translation vers l'Est des territoires importants de l'Empire Romain
Se rapprocher du cœur du christianisme, situé dans le Levant et en Anatolie.

Construction:
Par l'Empereur Constantin 1er
de 324 à 330

Particularités:
Ne constitue pas une sécession avec l'Empire Romain, Constantinople est la nouvelle capitale de l'Empire Romain.
La population atteint très vite les 200 000 habitants faisant de C. la ville la plus peuplée de l'Empire.

b. Rayonnement culturel
Universalisme romain/romanité => idéologie expansioniste et de réunification de l'Empire
Gouvernance des territoires de l'Empire Romain d'Occident relégué en théorie à des "Consuls" => Clovis et Théodoric.
Riche aristocratie byzantine finance le dévelloppement des arts et de la technologie
Jusqu'à la Renaissance, toutes les productions artistiques majeures de la Chrétienté sont produites dans les ateliers de Constantinople.
De manière générale, le rayonnement culturel de Constantinople est dû à son rôle religieux : L'Empereur est le lieutenant du Dieu des Chrétiens sur Terre et tous les sujets de Dieu, sont donc aussi, en théorie ceux de l'Empereur également.
Ce rayonnement s'étend jusqu'aux terres russes et normandes.
À noter : Anne Comnène, fille de l'Empereur Alexis 1er Comnène, fascinée par la chevalerie franque, dégoûtée par l'inculture de la cour, elle tente de mettre son mari sur le trône mais échoue. Elle dédie le reste de sa vie à écrire l'histoire de son père.

c. Agriculture et Commerce
- Agriculture
Organisation autour de Constantinople : multiples fermes plus ou moins autonomes fonctionnant autour d'une ferme centrale. 
Deux propriétaires principaux : L'Empereur et le clergé.
Technnique la plus répandue : Araire aux bœufs
- Commerce
Gains et pertes (surtout pertes) territoriales => fluctuations fiscales et grande instabilité de la monnaie.
La monnaie s'adapte aux peuples avec qui les Byzantins commercent.
Les Byzantins commercent avec toutes les peuples de l'Ouest Méditerranéen, mais importent des produits venant de Chine et en exportent d'autres jusqu'en Scandinavie.
À partir du XIIIe siècle les villes de Gênes et de Venise, après avoir été de simple partenaires commerciaux, s'implantent durablement dans la Ville => comptoirs commerciaux. 


d. Vikings
NOAH

e. Architecture, Arts et Urbanisme
Les Byzantins restent avant tout des Romains, ils en conservent certaines coutumes.
-> bains
-> cirques
-> hippodromes
-> rues pavées E/O N/S
Influence orientale => somptueux palais
Byzantins exportent leurs talents architecturaux et artistiques dans tout le monde chrétien, les Français, Italiens, Espagnols, Scandinaves et Russes commandent par exemple des mosaïques et portes d'églises. 
Un exemple remarquable d'urbanisme : la Mésè 
Rue centrale, passant par tous les forums de la ville
Particularité : Rue à portiques (11m/25m) (Rue de Rivoli)
Perle de Constantinople : Sainte-Sophie 
Construite sous Justinien (construction 532-537)
Prouesse architecturale : Coupole de 30m de diamètre pour 60 mètres de haut 
Solution : Physiciens et quadrature du cercle, comment répartir le poids de cette coupole sur des piliers disposés en carré ? 
Problème résolu avec grand esthétisme : piliers dissimulés, plus de 40 fenêtres percées rien que dans la coupole.
Histoire Ste-Sophie : NOAH

3. Période Turque
-----------------

a. Turcs et Grecs
Migrations des Turcs et rapide islamisation
Peuple cavalier cousin des Mongols => redoutables combattants
Au fil des siècles, leur implantation militaire et leur nombre ne saissant de croître par la migration fait qu'ils deviennent majoritaires en Anatolie, où le Sultanat de Rum (futurs Ottomans) s'est fondé.
Ils remplacent les populations helleniques.

b. L'évolution de Hagia Sophia / Ayasofya
Église Sainte-Sophie => Mosquée Ayasofya, immédiatement après prise de Constantinople
Mosquée => Musée, sous Atatürk (après que les Jeunes Turcs ont voulu la dynamiter) : laïcisation de la Turquie et promotion de l'universalisme occidental.

c. Istanbul aujourd'hui
Byzance, Constantinople, Constantiniyye ne sont plus, reste seulement Istanbul.
La ville a changé plusieurs fois de nom, de peuple, de culture, de religion. Elle a été à plusieurs reprises un centre religieux de religions différentes. Aujourd'hui Istanbul est la capitale de la République de Turquie et reflète aux yeux du monde les ambitions de cette nation émergente dont le poids est de plus en plus important dans la région.
Ainsi, depuis plus de 2600 ans, Byzance, Constantinople, Constantiniyye et Istanbul n'ont presque jamais cessé d'être des villes de puissance régionale, voire internationale.
