Constantinople

Constantinople est une ville qui n’existe plus aujourd’hui, à sa place, Istanbul se dresse, entre Asie et Europe, comme capitale de la Turquie ; mais autrefois, Constantinople était une ville grecque, puis romaine, avant de tomber aux mains des Turcs. 
Ainsi, il est judicieux de se poser la question suivante : 

Par quelles cultures Byzance a-t-elle été influencée et quelle a été son influence sur l'Europe et l'Asie durant plus de 2600 ans ?

I. Byzance ( -667 → 330 )

À l’origine, en -667, fut créée une colonie grecque dans le détroit du Bosphore, facile à défendre, et à un emplacement stratégique, elle fut l’un des piliers du commerce entre la Mer Noire et la Mer Méditerranée. 
Selon la légende, elle aurait été fondée par Byzas, un colon grec Mégarien, il serait, selon les sources, le fils de Neptune et de Cérossa, elle-même fille de Jupiter et de Io, ou alors celui d’une divinité Thrace et du (oui, un) nymphe Sémestre, d'autres liens de parenté lui sont attribués.

[plan de Byzance]

Byzance était donc bien une ville grecque, cependant, au premier siècle de notre ère, les Romains la conquièrent, ainsi que toute la Grèce. Elle devient au bout d'un certain temps un pôle de l'hellenisme dans la région.

II. Constantinople ( 330 -> 1453 )

Avec le déclin croissant de l'Empire Romain, le centre culturel, technologique, artistique et militaire s'est déplacé vers l'Est. C'est pourquoi, en l'an 330 de notre ère, l'empereur Constantin 1er décide de construire une nouvelle capitale romaine, sur ce qui reste de Byzance.
Cette capitale, Constantinople est bien romaine, elle permet une meilleure défense de cet Orient romain en pleine émulation. Elle est aussi plus proche du foyer du christianisme, religion de plus en plus importante, et à laquelle l'Empereur s'intéresse beaucoup.

Très vite la population de la ville atteint les 200 000 habitants et il est nécessaire de l'agrandir, cet accroissement de la ville reflète une expansion de l'Empire. 
L'universalisme romain, idéologie d'expansion et de réunification, place cette ville au centre de la politique et de l'histoire européenne. En effet, bien que Constantinople ne contrôle plus l'Occident, les rois qui accaparent ces territoires le fond avec "l'accord" de Constantinople et son nommés consuls. [INSERT LATIN] Le texte date du XIe siècle, l'auteur étudiait le grec dans l'église de Sainte-Sophie.
Le rayonnement culturel de Constantinople transparaît également par le fait qu'il y eut de nombreuses impératrices, Irène l'Athénienne, de Hongrie et bien d'autres...
Les arts et la technologie sont en plein essor grâce à une aristocratie riche qui finance leur dévellopement.
Constantinople demeure, jusqu'à la quatrième Croisade le centre religieux du monde chrétien, en effet, l'Empereur est le lieutenant de Dieu sur Terre, et tous les sujets de celui-ci sont donc aussi ceux de l'Empereur. 
Jusqu'à la Renaissance, Constantinople est également le centre artistique de toute l'Europe et au-delà, on commande des mosaïques et des portes d'églises aux ateliers byzantins depuis les quatres coins de la Méditerrannée et même depuis la Russie ou la Scandinavie.

L'agriculture autour de Constantinople est assez rudimentaire, de petites communautés de fermiers s'éparpillent le long d'un cours d'eau autour d'une plus grande ferme, le principal outil d'agriculture est l'araire au bœufs. Le tout appartenant, ou à l'Empereur, ou bien au clergé.
La monnaie dans l'Empire est très instable, en raison des nombreuses fluctuations financières dues aux évolutions territoriales.
Les Byzantins commercent avec tous les peuples de la Méditerranée Orientale, Arabes, Ostrogoths, Slaves... Mais aussi avec d'autres peuples venant de l'intérieur du continent.
Ils importent ainsi des produits venant de l'Extrême Orient comme d'Arabie pour les exporter en Europe. Constantinople se situe au centre de ces échanges, et son dévellopement y est lié.
À partir du XIIIe siècles, les marchands Gênois et Vénitiens s'installent durablement sur les terres de l'Empire et dans Constantinople même pour y commercer, parfois au détriment des marchands byzantins.

Les Vikings 
Quelques rappels :
Le mot Viking (du vieux norrois "Víkingr") désigne ceux qui prennent le large, hommes comme femmes, pour s'enrichir, par l'argent ou le sang, ce terme n'était pas restreint qu'aux Scandinaves, qui étaient en majorité des fermiers. 

Un clan particulier de "Vikings" étaient les Varègues, originaires de Suède, ils descendirent les fleuves russes jusqu'à Constantinople pour chercher les pièces d'argent du Serkland (le pays des Sarasins, ces pièces étaient arrivées en terres Scandinaves via le commerce.)
En chemin, ils fondèrent la ville de Novgorod (en 860), ils prirent la ville de Kiev (en 882) et se firent ainsi rois de la grande Rus', le premier étant Oleg le Sage, fils de Rurik. 
Ils effectuèrent également des raids sur Constantinople et les côtes anatoliennes de la Mer Noire à partir de 907. Ceux-ci cessent à la fin du siècle, lorsque s'épuisent les mines d'argent, la dernière expédition fut menée par Yngvar en 1041. 
Après le premier raid de 907 mené par Oleg le Sage, celui-ci et l'Empereur Léon VI le Sage (lui aussi), établirent des accords commerciaux en faveur des Varègues, ceux-ci gagnant le droit de commercer dans le territoire byzantin, et les Byzantins étant permis de recruter comme mercenaires des Varègues.

Le rayonnement de Constantinople ne fut pas de toute éternité, en effet dès le VIIe siècle son pouvoir et son autonomie décrurent violemment à cause de la perte de l'Égypte, de l'Afrique du nord et de la Sicile au profit des musulmans. 
Plus tard, le jour de Noël de l'an 800, c'est la légitimité byzantine en tant que puissance unificatrice de la romanité qui est ébranlée : Charlemagne se fait couronner Empereur d'Occident, par l'évèque de Rome, le Pape.
Ironiquement, ce ne sont pas les musulmans, les Perses, ou les Varègues qui enclenchèrent véritablement la chute de Constantinople, ce sont les chrétiens eux-mêmes.
En effet, lors de la quatrième Croisade, lancée par le Pape Innocent III, les croisés pillèrent Constantinople et l'église Sainte-Sophie en 1204.
L'empire est divisé en trois royaumes qui luttent pendant plus de 50 ans pour récouvrer le contrôle de Constantinople, occupée par les "Latins". 
Les routes commerciales, autrefois incontestablement byzantines, sont accaparées par les villes de Gênes et Venise.
À partir de ce moment, les byzantins perdent le contrôle sur l'Anatolie, laissant la place et temps aux Turcs de s'y installer...
Ce sont finalement ces Turcs Ottomans qui achevèrent définitivement l'aventure romaine.
Un premier siège est mené par le Sultan Bajazet en 1391, il est interrompu par l'arrivée des croisés Hongrois, ceux-ci écartés, le siège repris pour s'arrêter de nouveau après la capture du sultan par Tamerlan.
En 1444, une croisade est lancée par l'Union de Pologne-Lithuanie et de Hongrie, pour tenter de stopper l'avancée turque en Europe, et par là même, sauver Constantinople, à la bataille de Varna cette alliance est brisée, et le glas sonne pour les byzantins.
Le 29 mai 1453, après un long siège, et alors que les légendaires murailles de Constantinople ont été détruites par les canons Turcs, la Ville est prise par Mehmed II, qui en fait aussitôt la capitale de son empire.

Sainte-Sophie / Hagia Sophia 
[INSERT PHOTOS]
Construite pour la première fois sous Constantin en 325 sur les ruines du temple grec d'Apollon. En 360 elle est sacrée par Constance II Grande Église, "Megalè Eklèsia".
Son nom est issu du grec "Hagia Sophia" qui signifie Sainte Sagesse (et non pas Sainte-Sophie).
Après avoir été brulée en 404 elle est reconstruite par Justinien, selon les plans d'un mathématicien et d'un architecte qui s'inspirent du panthéon de Rome et de l'art primitif chrétien.
Durant 5 ans, plus de 10 000 ouvriers travailleront sur les chantiers de la Grande Église et les pierres des anciens monuments grecs sont utilisées, ce qui explique l'absence de ruines.
La ville de Constantinople étant située sur un point sensible entre deux plaques tectoniques, de nombreux tremblements de terre endommagèrent la coupole de Sainte-Sophie au cours des siècles (556/557/558/740/869/989/1231/1237/1334), elle fut systématiquement reconstruite.
Elle fut pillée en 1204 par les croisés, tandis qu'elle reste relativement intacte quand les Turcs prennent Constantinople en 1453. 
En effet le Sultan Mehmed II, qui la convertit en mosquée, y est particulièrement attaché, tout comme Soliman le Magnifique. 
En 1934, le grand réformateur Mustafa Kemal Atatürk, dans un élan d'occidentalisation et de laïcisation fait de la mosquée un musée.
En 1985, le monument est inscrit au patrimoine mondial de l'UNESCO
Le 10 juillet 2020, le président de la république de Turquie Erdogan, fait du musée une mosquée de nouveau, cette réforme est très populaire dans le pays.

La construction de Sainte-Sophie présentait un défi majeur : Comment faire tenir une coupole de base circulaire sur quatres piliers formant un carré ?
Il fallait résoudre la quadrature du cercle. D'éminents matématiciens et physiciens furent dépêchés sur la question et l'on trouva une solution. 
[INSERT PHOTO] 
La coupole mesure ainsi 30 mètres de diamètre, pour 60 mètres de haut. 
Les matériaux, très divers, venaient des quatres coins de l'Empire, et de pays lointains.
De manière plus générale, dans les arts urbains, les byzantins gardent les coutumes romaines :
Les bains romains sont conservés, de nouveaux sont construits, mais à partir du XIe - XIIe siècle il perdent leur fonction récréative sous la pression de l'Église. 
Les hippodromes sont également très populaires.
Un exemple remarquable d'urbanisme : la Mésè
Rue centrale, passant par tous les forums de la ville sa particularité est d'être une à portiques 

III. Période Turque ( 1453 - Aujourd'hui)
Les Turcs remplacèrent les populations hellènes en Anatolie
La ville a changé plusieurs fois de nom, de peuple, de culture, de religion. Elle a été à plusieurs reprises un centre religieux de religions différentes.
Aujourd'hui Istanbul est la capitale de la République de Turquie et reflète aux yeux du monde les ambitions de cette nation émergente dont le poids est de plus en plus important dans la région.

Ainsi, depuis plus de 2600 ans, Byzance, Constantinople, Constantiniyye et Istanbul n'ont presque jamais cessé d'être des villes de puissance régionale, voire internationale.
                                 
