2.a. Essor
Carte expansion Justinien
Image Justinien

2.b. Rayonnement cult: 3 photos
Carte contenant : Empire Byzantin, Royaume de Theodoric, Royaume de Clovis
Empereur lieutenant de Dieu sur Terre
Photo de Anne Comnène

2.c. Agriculture et Commerce: 3-7p
Photo d'un araire aux bœufs
Photo pièce de monnaies byzantines (ex: nomisma, millasèrion, folis)
Carte montrant Gênes et Venises ainsi que leurs possessions territoriales

2.e. Architecture, Arts et Urbanisme: env. 10 
Photos de monuments (bains, cirques, hippodromes, rues pavées, Menès)
Photos mosaïques, églises, frontons
Photos Sainte-Sophie (ext et int)
Plan architectural de Sainte-Sophie

3.a. Turcs et Grecs
Carte montrant évolution éthnique en Anatolie
3.b. Photos intérieur Ayasofya
3.c. Skyline Istanbul
