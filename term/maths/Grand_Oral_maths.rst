Comment construire des nombres à la règle et au compas et quels sont-ils ?

I. Intro

a. Remarque générale
b. Courte histoire
- règles de géométrie des Grecs Anciens
c. Règle non graduée et compas

II. Réponse

a. Constructibilité
- point
- segments : deux points extrémités constructibles
- nombre : longueur segment constructible
b. Opérations simples
- addition
- soustraction
- multiplication
- division
- racine carrée

III. Conclusion

a. 5 opérations élémentaires : constructible 
- car racine d'un polynôme de degré minimal 2 (Wantzel)

b. Limites
- Pi et cuadrature du cercle
