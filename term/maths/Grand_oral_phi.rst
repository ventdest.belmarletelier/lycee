D'où vient Phi et où le trouve-t-on ?


I/ [La suite de Fibonacci] (2 min)
 - courte présentation de Fibonacci
Fibonacci est un matématicien Pisan du XII et XIIIe siècle
Il a reçu son éducation en Algérie
1202 : Liber Abaci (Al-Kwarizmi, suites numériques, Euclide)
 - retour à la suite de Fibonacci
contexte (évolution nombre de lapins)
     1. un couple de lapins n'est pas en âge de se reproiduire pendant ses deux premiers mois
     2. au troisième mois le couple donne naissance à un nouveau couple


II/ [Phi, le nombre d'or] (2 min)
 - observation : 1, 1, 2, 3, 5, 8, 13, ...
 - lien avec le nombre d'or (connu depuis l'Antiquité) : la raison de la suite tend vers environ 1,618, le nombre d'or
 - expliquation simple de ce qu'est le nombre d'or (rapport entre deux côtés du rectangle "d'or")
 - Méthode d'Al-Kwarizmi x² = x+1 (démo géométrique + résolution polynôme => valeur exacte (1+sqrt(5))/2)


III/ [Où le trouve-t-on ?] (1 min)
 - art : divine proportion
Ex : Parthénon d'Athènes, taleaux Renaissance, portes
 - nature : spirales de pommes de pin (5-8 ou 8-13), plantes en général


Conclusion : Nombre d'or dans la nature => évolution du nombre de lapins , la boucle est bouclée !
