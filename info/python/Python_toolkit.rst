.. use rst2html5 to create Slides
.. w|!clear; rst2html5 --deck-js --pretty-print-code --embed-content python_toolkit.rst> python_toolkit_slides.html

===============
Python Toolkit
===============
:Authors:
    Luis Belmar-Letelier <luis.belmar-letelier@mazars.fr>
:Version: 2020-02

:Training reviewer:
    Gilles Cuyaubère <gcuyaubere@zettafox.com>,
    Nathali <nathali.paz-del-castillo@mazars.fr>,


High level datastructures: strings, list, tuples and dictionaries
===================================================================
.. code:: python

  ## Strings, lists and dictionaries
  s, l, d = 'hello', [22, 3.14, 'world'], {'age': 33, 'name': 'Luis'}
  l, s, d
  # ([22, 3.14, 'world'], 'hello', {'age': 33, 'name': 'Luis'})

  ## Tuples
  t = (22, 3.14, 'hello')
  t
  # (22, 3.14, 'hello')

Accessing those data structures
---------------------------------
.. code:: python

  ## By indexes starting by 0 [idx:idx]
  s[0], s[1], s[0:4]
  # ('h', 'e', 'hell')

  l[1], l[1] / 10 + 2**3
  # (3.14, 8.314)

  ## By negative indexes : get in reverse order
  l[-1], s[:-2], s[-4]
  # ('world', 'hel', 'e')

  ## Dict are unordered lists of values indexed by strings keys
  d
  # {'age': 33, 'name': 'Luis'}

  d['age'], d['age'] + 7, d['name'] + " Arturo"
  # (33, 40, 'Luis Arturo')

String
-------

String (multiple lines)
~~~~~~~~~~~~~~~~~~~~~~~~
.. code:: python

  # Use triple double quote
  haiku1 = """La pluie commence à tomber
  c'est le battement
  du cœur de la nuit"""

  # Accumulate strings
  haiku2 = "La pluie commence à tomber\n"
  haiku2 += "c'est le battement\n"
  haiku2 += "du cœur de la nuit"

  # Better: use brace continuation, please avoid the two above
  haiku3 = ("La pluie commence à tomber\n"
            "c'est le battement\n"
            "du cœur de la nuit")

  haiku1 == haiku2 == haiku3
  # True

String templating with `f-string`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

As of Python 3.6, **f-strings** are a great new way to format strings. Not only
are they more readable, more concise, and less prone to error than other ways
of formatting, but they are also faster!

.. code:: python

  val = '54.4'  # the value

  f"{val}"
  # '54.4'

  f"{val:%<9}"
  # '54.4%%%%%'

  f"{val:0>9}"
  # '0000054.4'

  f'=={s.upper():^16}=='  # **^** Forces the field to be centered
  # ==     HELLO      ==

More on `f-string`
~~~~~~~~~~~~~~~~~~~
- Formatted string literals (also called f-strings for short)

  - ref: https://docs.python.org/3.8/reference/lexical_analysis.html#f-strings

All variables present in the namespace are usable inside **{}** if the string
is an *f-string*:

.. code:: python

  table = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 7678}
  for name, phone in table.items():
     print(f'{name:10} ==> {phone:10d}')
  # Sjoerd     ==>       4127
  # Jack       ==>       4098
  # Dcab       ==>       7678

On precedent Python version the above whould be write this way:

.. code:: python

  ## format with left padding
  "{}".format('54.4')
  # '54.4'

  ## format with left padding
  "{:%<9}".format('54.4')
  # '54.4%%%%%'

  ## **>** Forces the field to be right-aligned
  "{:0>9}".format(54.4) #
  # '0000054.4'

  print('=={:^16}=='.format('hello'))
  # ==     hello      ==

ref: https://docs.python.org/3.8/library/string.html#format-specification-mini-language

Data structures have their own API
-------------------------------------
.. code:: python

  ## string API
  string_api = """
  s.capitalize   s.format_map   s.isnumeric    s.maketrans    s.split
  s.casefold     s.index        s.isprintable  s.partition    s.splitlines
  s.center       s.isalnum      s.isspace      s.replace      s.startswith
  s.count        s.isalpha      s.istitle      s.rfind        s.strip
  s.encode       s.isascii      s.isupper      s.rindex       s.swapcase
  s.endswith     s.isdecimal    s.join         s.rjust        s.title
  s.expandtabs   s.isdigit      s.ljust        s.rpartition   s.translate
  s.find         s.isidentifier s.lower        s.rsplit       s.upper
  s.format       s.islower      s.lstrip       s.rstrip       s.zfill
  """

  ## list API
  list_api = """
  l.append  l.count   l.insert  l.reverse
  l.clear   l.extend  l.pop     l.sort
  l.copy    l.index   l.remove
  """

  ## dict API
  dict_api = """
  d.clear      d.get        d.pop        d.update
  d.copy       d.items      d.popitem    d.values
  d.fromkeys   d.keys       d.setdefault
  """

Please take the time to go at least one time throught every method of this
lists.

All python builtins : __builtins__
======================================
- ref: http://docs.python.org/release/2.7/library/functions.html

Namespace
----------
.. code:: python

  >>> dir()
  ['__builtins__', '__doc__', '__name__', '__package__', 'd', 'haiku3', 'l', 's',
   't']

  >>> dir(__builtins__), len(dir(__builtins__))
  (['ArithmeticError', 'AssertionError', 'AttributeError', '...',
   'TabError', 'True', 'ValueError', 'Warning', 'ZeroDivisionError',
   '__debug__', '__doc__', '__name__', '__package__', 'abs',
   'any', 'apply', 'basestring', 'bin', 'bool', 'buffer', 'bytearray',
   'bytes', 'callable', 'chr', 'classmethod', 'cmp', 'coerce', 'compile',
   'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'divmod',
   'enumerate', 'eval', 'execfile', 'exit', 'file', 'filter', '...'],
   144)

Let's explore python __builtins__
-----------------------------------
.. code:: python

  explore = [b for b in dir(__builtins__) if not b[0].isupper()
                                              if not b.startswith('_')]
  print(explore)
  # ['abs',        'all',        'any',         'ascii',        'bin',     'bool',
  #  'breakpoint', 'bytearray',
  #  'bytes',      'callable',   'chr',         'classmethod',  'compile', 'complex',
  #  'copyright',  'credits',    'delattr',     'dict',         'dir',     'display',
  #  'divmod',     'enumerate',  'eval',        'exec',         'filter',  'float',
  #  'format',     'frozenset',  'get_ipython', 'getattr',      'globals', 'hasattr',
  #  'hash',       'help',       'hex',         'id',           'input',   'int',
  #  'isinstance', 'issubclass', 'iter',        'len',          'license', 'list',
  #  'locals',     'map',        'max',         'memoryview',   'min',     'next',
  #  'object',     'oct',        'open',        'ord',          'pow',     'print',
  #  'property',   'range',      'repr',        'reversed',     'round',   'set',
  #  'setattr',    'slice',      'sorted',      'staticmethod', 'str',     'sum',
  #  'super',      'tuple',      'type',        'vars',         'zip']

  >>> len(explore)
  73

Let's group them all
-----------------------

:Data Types:
  ``bool``, ``int``, ``float``, ``str`` , ``bin`` , ``oct`` ,
  ``hex``, ``bytearray``, ``unicode``, ``type``, ``bytes`` , ``basestring``, ``ord``

:Functional programming:
  ``filter`` , ``map``, ``reduce``, ``apply``, ``all``, ``any``,

:Math functions:
  ``abs``, ``complex``, ``divmod``, ``max``, ``min``, ``round``, ``sum``,
  ``pow``

:Data Structures:
  ``dict``, ``list``, ``enumerate``, ``set``, ``frozenset``, ``tuple``

:List:
  ``zip``, ``sorted``, ``next``,  ``range``, ``xrange``

:File functions:
  ``file``, ``open``

:String functions:
  ``slice``, ``format``, ``unichr``, ``chr``, ``reversed``, ``len``

:Object oriented:
  ``classmethod`` , ``isinstance``, ``property``, ``object``, ``getattr``,
  ``setattr``, ``hasattr``, ``delattr``, ``staticmethod``, ``super``,
  ``globals`` , ``locals``, ``vars``

:Licensing:
  ``license``, ``copyright``

:Print:
  ``print``, ``repr``, ``dir``

:Other functions:

  ``callable``, ``coerce``, ``buffer`` , ``cmp``, ``exit``, ``hash``, ``help``,
  ``quit``, ``raw_input``,  ``callable`` , ``compile``, ``credits`` , ``eval``,
  ``execfile`` , ``intern`` , ``memoryview``, ``reload``,  ``unichr``


Please like for `list`, `dict` and `str` API, please take the time to go at
least one time throught every method of this lists.

Control structures
=====================
Ref: https://docs.python.org/3.8/tutorial/controlflow.html

Let's take a list:

.. code:: python

  dl = ['z', 'o', 'e', 3, 7, 'Luis', 'RR']
  ll = l + list(dl); print(ll)
  # [22, 3.14, 'world', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']

a = X if cond1 else Y
-----------------------
.. code:: python

  name = 'Luis'
  long_name = name if len(name) > 8 else False; long_name
  # False

  name = 'Leopoldine'
  long_name = name.upper() if len(name) > 8 else False; long_name
  # 'LEOPOLDINE'

`for` loop
------------
.. code:: python

  ll
  # [22, 3.14, 'world', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']

  for i in ll:
      print('.', end=',')
  # .,.,.,.,.,.,.,.,.,.,

if, elif, elif, ...,  else
----------------------------
.. code:: python

  for i in ll:
      if isinstance(i, int):
          print(f"Yes '{i}' is an int")
      elif isinstance(i, str):
          if i in ['world', 'luis']:
              print(f"String in the list: '{i}' -> ok")
      else:
          print(f"Not an int, not a string: {i} ;( ")
  # Yes '22' is an int
  # Not an int, not a string: 3.14 ;(
  # String: 'world' -> ok
  # Yes '3' is an int
  # Yes '7' is an int


Looping with comprehension list
---------------------------------
ref: https://docs.python.org/3.8/tutorial/datastructures.html#tut-loopidioms

An usual patern is to create a list from some other list:

.. code:: python

  ll
  # [22, 3.14, 'world', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']

  res =[]
  for i in ll:
      if not isinstance(i, str):
          res.append((i, i-1))
  print(res)
  # [(22, 21), (3.14, 2.14), (3, 2), (7, 6)]

  res =[]
  for i in ll:
      if isinstance(i, str):
          res.append(i.upper())
  print(res)
  # ['WORLD', 'Z', 'O', 'E', 'LUIS', 'RR']

With comprehension list the two aboves can be done in one very clear line:

.. code:: python

  ll
  # [22, 3.14, 'world', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']

  [(i, i-1) for i in ll if not isinstance(i, str)]
  # [(22, 21), (3.14, 2.14), (3, 2), (7, 6)]

  [i.upper() for i in ll if isinstance(i, str)]
  # ['WORLD', 'Z', 'O', 'E', 'LUIS', 'RR']

Exercice: Return a list of `n` first prime numbers using list comprehention

.. code:: python

  n = 14
  [x for x in range(2, n) if all([x%i != 0 for i in range(2, x)])]
  # [2, 3, 5, 7, 11, 13]  # Bingo !

Explanation:

.. code:: python

  ## `all` Return True if bool(x) is True for all values x in the iterable.
  all([1, 2, 3, 3, 'ok'])
  # True
  all([1, 2, 0, 3, 'ok'])
  # False
  all([1, 2, 3, False, 'ok'])
  # False

  x = 6
  [(i, x%i != 0) for i in range(2, x)]
  # [(2, False), (3, False), (4, True), (5, True)]
  all([False, False, True, True])
  # False

  x = 5
  [(i, x%i != 0) for i in range(2, x)]
  # [(2, True), (3, True), (4, True)]
  all([True, True, True])
  # True

Dictionaries comprehention
----------------------------
Dict comprehensions can be used to create dictionaries from arbitrary key and
value expressions:

.. code:: python

  dd = {'a': 42, 'pi': 3.14, 'nom': 'Luis', 'age': 33}

  {k:v+1 for k, v in dd.items() if k not in ['nom']}
  # {'a': 43, 'pi': 4.140000000000001, 'age': 34}

  {k:int(v+10) for k, v in dd.items() if k not in ['nom']}
  # {'a': 52, 'pi': 13, 'age': 43}

  ## Creating dict more in comprehensions' way
  {k: f'file_{k}' for k in range(4)}
  # {0: 'file_0', 1: 'file_1', 2: 'file_2', 3: 'file_3'}

  ## Creating dict from list of tuples and keywords
  lt = [('a', 42), ('pi', 3.14)]
  dict(lt, nom='Luis', age=33)
  # {'a': 42, 'age': 33, 'nom': 'Luis', 'pi': 3.14}

  ## Creating dict from a dict and keywords
  d
  # {'age': 33, 'name': 'Luis'}

  dd = d.copy()
  dd.update(name='Paul', age=42, town='Paris')
  # {'age': 42, 'name': 'Paul', 'town': 'Paris'}

  lt
  # [('a', 42), ('pi', 3.14)]

  dict(lt, **dd)
  # {'a': 42, 'pi': 3.14, 'age': 42, 'name': 'Paul', 'town': 'Paris'}



Functions, methods and classes
================================
It's easy in Python functions to handle **default values** for arguments and
**variable number of arguments**:

.. code:: python

  def plus(x, y=3, *a_list):
      # default argument should follows non-default argument
      print(a_list)
      print(f'{x} + {y} = {x+y}')

  plus(7, 14, 'a', 'coucou', 56)
  # ('a', 'coucou', 56)
  # 7 + 14 = 21

Note that `*[]` unpack the list content:

.. code:: python

  plus(7, 14, *['a', 'coucou', 56])
  # ('a', 'coucou', 56)
  # 7 + 14 = 21

**Named arguments** have to be define after un-named arguments:

.. code:: python

  def plus(x, y=3, nom='luis', age=52, ville=None):
      print(f'Nom: {nom}, Age: {age}, Habite à: {ville}')
      print(f'{x} + {y} = {x+y}')

  plus(7, 14, 'a')
  # Nom: a, Age: 52, Habite à: None
  # 7 + 14 = 21

Variable number of **named arguments** are handeled throught dictionaries:

.. code:: python

  def my_plus(x, y=2, *optional_list, **optional_dict):
    "my_plus illustrates a generic python function"

    # 'y' has the default value: 2
    print(f'{x} plus {y} is {x+y}')

    # optional_list is ... optional
    if optional_list:
        print(f'list: len: {len(optional_list)} '
              f'items: {optional_list}')
    # optional_dict is ... optional
    if optional_dict:
        print(f'dict:\n'
              f'  keys: {list(optional_dict.keys())}\n'
              f'  values: {list(optional_dict.values())}')

  my_plus(64, 12, 'aa', 'zz', town='Shanghai', note='AAA')
  # 64 plus 12 is 76
  # list: len: 2 items: ('aa', 'zz')
  # dict:
  #   keys: ['town', 'note']
  #   values: ['Shanghai', 'AAA']

.. code:: python

  my_plus(34)
  # 34 plus 2 is 36

  my_plus(44, 4, 'a', 'b', 'c')
  # 44 plus 4 is 48
  # list: len: 3 items: ('a', 'b', 'c')

  my_plus(54, town='Paris', age=33)
  # 54 plus 2 is 56
  # dict:
  #   keys: ['town', 'age']
  #   values: ['Paris', 33]

  my_plus(64, 12, 'aa', 'zz', town='Shanghai', note='AAA')
  # 64 plus 12 is 76
  # list: len: 2 items: ('aa', 'zz')
  # dict:
  #   keys: ['town', 'note']
  #   values: ['Shanghai', 'AAA']


Classes, constructors, instances
---------------------------------
.. code:: python

  class Pet():
      # The constructor is the method named __init__
      def __init__(self, name, food='eggs'):
          # name and food are instance variables
          self.name = name
          self.food = food
      # show_msg is a class variable
      show_msg = "I am {name}, a Pet, I eat {food}"
      # A class method is just a fonction with self as first variable
      #   self refer to the future instance
      def show(self, msg=None):
          ns = dict(name=self.name, food=self.food)
          if not msg:
              print(Pet.show_msg.format(**ns))
          else:
              print(msg.format(**ns))

  pets = [Pet(name) for name in ['Bob', 'Mick', 'Yan', 'Tony']]
  for pet in pets:
      pet.show()
      if len(pet.name) > 3:
          pet.food = 'bacon'
          pet.show(' Hey, now {name} eats {food}')

  # I am Bob, a Pet, I eat eggs
  # I am Mick, a Pet, I eat eggs
  #  Hey, now Mick eats bacon
  # I am Yan, a Pet, I eat eggs
  # I am Tony, a Pet, I eat eggs
  #  Hey, now Tony eats bacon

Implementing data objects
=============================
`typing.NamedTuple` unmutable data objects
---------------------------------------------

Need to lock down field names to avoid typos we can use `typing.NamedTuple`
classes:

.. code:: Python

  from typing import NamedTuple
  class Car(NamedTuple):
      mileage: float
      automatic: bool
      color: str = 'purple'
  car1 = Car("red", 3812.4, True)

  # # Instances have a nice repr:
  car1
  # Car(color='red', mileage=3812.4, automatic=True)

  # # Accessing fields:
  car1.mileage
  # 3812.4

Shorten alternative syntaxe:

.. code:: Python

  from typing import NamedTuple
  Guy = NamedTuple('Guy', name=str, id=int)
  gg = Guy(name='Luis', id=42)
  print(f'Name: {gg.name}, id: {gg.id}')
  # Name: Luis, id: 42


This NamedTuple can be used to *type hints*

.. code:: Python

  gg = Guy(name='Luis', id=42)
  g2 = Guy(name='Mario', id=33)
  print(f'Name: {gg.name}, id: {gg.id}')
  # Name: Luis, id: 42

  def plus(a: Guy, b: Guy) -> tuple:
          print(a, b)
          return f'{a.name}__{b.name}', a.id * b.id

  out = plus(gg, g2)
  # Guy(name='Luis', id=42) Guy(name='Mario', id=33)
  print(out)
  # ('Luis__Mario', 1386)


Note: do not use the old namedtuple: `collections.namedtuple`.

Note: In programming in general, anything that CAN be immutable SHOULD be
immutable. We gain two things:

- Easier to read the program, we don't need to worry about values changing,
  once it's instantiated, it'll never change (namedtuple)
- Less chance for weird bugs

In case we need mutability you should use `DataClass`  e.g.:

`dataclass` data objects with muttable fields
-----------------------------------------------
Often we need to lock down field names to avoid typos. Then we can use
`typing.NamedTuple` classes:

.. code:: Python

  from dataclasses import dataclass
  @dataclass
  class Guy:
      name: str
      age: int = 42
  g3 = Guy(name='Luis')
  print(g3)
  # Guy(age=42, name='Luis')
  g3.age += 3
  g3.age
  # 45


More high level datastructures: set, deque, sortedlist
========================================================

`sets`: unordered collection of unique elements
-------------------------------------------------
.. code:: python

  ens = set(['a', 3, 22, (41, True)])
  ens.intersection([3, 33, 'a'])
  # {3, 'a'}

  ## set API
  dict_set = """
  ens.add                         ens.issubset
  ens.clear                       ens.issuperset
  ens.copy                        ens.pop
  ens.difference                  ens.remove
  ens.difference_update           ens.symmetric_difference
  ens.discard                     ens.symmetric_difference_update
  ens.intersection                ens.union
  ens.intersection_update         ens.update
  ens.isdisjoint
  """

Let's create a small HR application in 30 python lines.

.. code:: python

  ## set Application
  engineers = set(['John', 'Jane', 'Jack', 'Janice'])
  programmers = set(['Jack', 'Sam', 'Susan', 'Janice'])
  managers = set(['Jane', 'Jack', 'Susan', 'Zack'])

  ## union, intersection and difference
  employees = engineers.union(programmers).union(managers) # union
  employees = engineers | programmers | managers           # union with |

  engineering_management = engineers & managers            # intersection
  fulltime_management = managers - engineers - programmers # difference

Now we hire 'Marvin' a new employee, update and check our employee set:

.. code:: python

  engineers.add('Marvin')                                  # add element
  engineers
  # Set(['Jane', 'Marvin', 'Janice', 'John', 'Jack'])

  ## issuperset and update
  employees.issuperset(engineers)     # superset test
  # False
  employees.update(engineers)         # update from another set
  employees.issuperset(engineers)
  # True

Then we let 'Susan' to go of

.. code:: python

  ## discard
  employees.issuperset(engineers)     # superset test
  for group in [engineers, programmers, managers, employees]:
      group.discard('Susan')          # unconditionally remove element
      print(group)

  # Set(['Jane', 'Marvin', 'Janice', 'John', 'Jack'])
  # Set(['Janice', 'Jack', 'Sam'])
  # Set(['Jane', 'Zack', 'Jack'])
  # Set(['Jack', 'Sam', 'Jane', 'Marvin', 'Janice', 'John', 'Zack'])

`collection.deque`: bidirectional fixed size endpoints accessible list
--------------------------------------------------------------------------
.. code:: python

  from collections import deque
  dl = deque([3, 7, 2, 'Luis'], maxlen=7)

  # appendleft, append, pop(left), remove, extend
  dl.appendleft('LL'); dl.append('RR'); dl
  # deque(['LL', 3, 7, 2, 'Luis', 'RR'], maxlen=7)

  dl.popleft()
  'LL'
  dl
  # deque([3, 7, 2, 'Luis', 'RR'], maxlen=7)

  # remove, extend
  dl.remove(2)
  dl
  # deque([3, 7, 'Luis', 'RR'], maxlen=7)

  dl.extend(['z', 'o', 'e']); dl
  deque([3, 7, 'Luis', 'RR', 'z', 'o', 'e'], maxlen=7)

  # rotate
  dl.rotate(-4); print(dl)
  # deque(['z', 'o', 'e', 3, 7, 'Luis', 'RR'], maxlen=7)

`collections.Counter` Multiset to count hashables items
----------------------------------------------------------
.. code:: python

  from collections import Counter
  c = Counter('abcdeabcdabcaba')
  c.most_common()
  [('a', 5), ('b', 4), ('c', 3), ('d', 2), ('e', 1)]
  ''.join(c.elements())
  # 'aaaaabbbbcccdde'

  ''.join(sorted(c.elements()))   # list elements with repetitions
  # aaaaabbbbcccdde


Advanced Control Structures
=============================
Ref: https://docs.python.org/3.8/tutorial/controlflow.html

The `else` clause of `for` loop
--------------------------------
.. code:: python

  for i in range(3):
      print(i)
  else:
      print('executed after the end of for iteration')
  # 0
  # 1
  # 2
  # executed after the end of for iteration

`break` and `continue` in a `for`
-----------------------------------

In a `for` loop `break` stop looping and avoid `else`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.. code:: python

  for i in range(12):
      print('.', end=' ')
      if i < 6:
         print(i, end=' ')
      else:
         break
  else:
      print('\nBecause of the `break` the `else` will not be executed')
  # . 0 . 1 . 2 . 3 . 4 . 5 .

In a `for` loop `continue` continue looping but skip inner evaluation
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
A `continue` statement executed in the innermost suite enclosing the `for` loop
skips the rest of the suite and goes back to testing the expression.

.. code:: python

  for i in range(12):
      print('.', end=' ')
      if i < 6:
         print(i, end=' ')
      else:
         print('+', end=' ')
         continue
         print('This will never be printed')
  else:
     print('\nAt the end of the loop the else is executed')

  # . 0 . 1 . 2 . 3 . 4 . 5 . + . + . + . + . + . +
  # At the end of the loop the else is executed

.. code:: python

  for i in range(4):
      print('.', end=',')     # <= the looping is not stoped by `continue`
      if i%2 == 0:
          print(f'{i} is even')
          continue
      print(f'{i} is odd')    # <= executed only if not `continue`
  else:
      print('else stuff')     # <= `continue` do not skip the `for else`
  # .,0 is even
  # .,1 is odd
  # .,2 is even
  # .,3 is odd
  # else stuff

Note: `continue` does not impact the execution of the `for else` statement if any.

The `While` statement with `break` and `continue`
---------------------------------------------------
General pattern::

  while assignment_expression:
     suite

.. code:: python

  a, b = 0, 1
  while a < 1000:
       print(a, end=',')  # `end=` to avoid new line on each iteration
       a, b = b, a+b
  # 0,1,1,2,3,5,8,13,21,34,55,89,144,233,377,610,987,

A `break` statement executed in the suite terminates the loop, the rest of the
suite is not executed

.. code:: python

  a, b = 0, 1
  while a < 1000:
       a, b = b, a+b
       print(a, end=',')
       if a > 60:
           print('more than 60')
           break
       print('=', end=',')  # <= this will not be printed after 55

  # 1,=,1,=,2,=,3,=,5,=,8,=,13,=,21,=,34,=,55,more than 60

A `continue` statement executed in the suite skips the rest of the suite
and goes back to testing the expression.

.. code:: python

  a, b = 0, 1
  while a < 1000:
       a, b = b, a+b
       if a < 100:
           print(a, end=',')
           continue
           print('This will not be printed (because of the continue)')
       print('+', end=',')
  # 1,1,2,3,5,8,13,21,34,55,89,+,+,+,+,+,+,

Tips
=====

Functional
------------
.. code:: python

  def hello(x):
      return x * x - 123

  map(hello, range(12))
  # <map object at 0x7f473ea71470>

  >>>list(map(hello, range(12)))
  # [-123, -122, -119, -114, -107, -98, -87, -74, -59, -42, -23, -2]

  def hello2(x, y):
     return x * y + 10

  # reduce was moved to functools
  from functools import reduce
  reduce(hello2, range(12)
  # 685883120

  list(range(12))
  # [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]

Create ranges
---------------
.. code:: python

  [i/10. for i in range(12)]
  # [0.0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0, 1.1]

  [i/10. for i in range(1, 11, 2)]
  # [0.1, 0.3, 0.5, 0.7, 0.9]

  range(1, 11, 2)
  # [1, 3, 5, 7, 9]

  [i/10. for i in range(1, 13, 2)]
  # [0.1, 0.3, 0.5, 0.7, 0.9, 1.1]

Translate strings
------------------
.. code:: python

  intab =  u"ÀÁÂÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÇçÌÍÎÏìíîïÙÚÛÜùúûüÿÑñ"
  outtab = u"aaaaaaaaaaaaooooooooooooeeeeeeeecciiiiiiiiuuuuuuuuynn"
  trantab = dict((ord(a), b) for a, b in zip(intab, outtab))
  # print(...) in Python 3.x
  print("Bônjour lés àmïs ça vÂ".translate(trantab))
  # Bonjour les amis ca va

Handle password hashing
------------------------
- passlib: password hashing library for Python
- http://code.google.com/p/passlib/

Use yml
------------
Install ruamel::

  $ pip install ruamel.yaml
  $ # There also is a commandline utility yaml available after installing:
  $ pip install ruamel.yaml.cmd

.. code:: python

  st = """
  # An employee record
  name: Example Developer
  job: Developer
  skill: Elite
  employed: True
  foods:
      - Apple
      - Orange and red
      - Strawberry
      - Mango and xxx
  languages:
      ruby: Elite
      python: Elite
      dotnet: Lame
  """

Now we can load it:

.. code:: python

  yaml.load(st)
  # {'employed': True,
  #  'foods': ['Apple', 'Orange and red', 'Strawberry', 'Mango and xxx'],
  #  'job': 'Developer',
  #  'languages': {'dotnet': 'Lame', 'python': 'Elite', 'ruby': 'Elite'},
  #  'name': 'Example Developer',
  #  'skill': 'Elite'}

To load from a file e.g. "test.yml":

.. code:: python

  with open("test.yml") as yamlfile:
      conf = yaml.load(yamlfile)


Add thousands separator to numbers
---------------------------------------
To do so, simply write::

  number = 192334494.70
  f'{number:,}'
  # 192,334,494.70

  f'{number:,}'.replace(',', ' ')
  # 192 334 494.70

note that this transforms the number to a string and can't be used to compute
values

R code to iterate on list of dict
------------------------------------
.. code:: R

  > nr <- conf$newrun
  > line <- nr[16]
  >
  > line
  [[1]]
  [[1]]$launchCOMPARATOR
  [1] 201510
  > names(line[[1]])
  [1] "launchCOMPARATOR"
  >
  > line[[1]][[names(line[[1]])]]
  [1] 201510
  >

..  Have Fun with API
..
.. passlib: password hashing library for Python
..   http://code.google.com/p/passlib/
..

.. "toc with :w|!clear;printf '\n\n\n\n\n'; grep -E "^\#|^ $" %

subprocess
------------
From bash::

  $ printf '  Number of xml files: %s\n' `find . -name '*.xml' | wc -l`
    Number of xml files: 4
  $

Now from python::

  import subprocess
  cmd = "printf '  Number of xml files: %s\n' `find . -name '*.xml' | wc -l`"

`run()` does not capture stdout or stderr by default::

  out = subprocess.run(cmd, shell=True)
  # Number of xml files: 4
  out.returncode
  # 0

`subprocess.check_output` is a shortcut:

.. code:: python

  out = subprocess.check_output(cmd, shell=True)
  print(out.decode('utf8'))
  # Number of xml files: 4

  str(out, 'utf8')
  # Number of xml files: 4

`subprocess.check_output` is equivalent to `run` with stdout define as PIPE:

.. code:: python

  out = subprocess.run(cmd, shell=True, stdout=subprocess.PIPE)
  str(out.stdout, 'utf8')
  ' Number of xml files: 4\n'

How to avoid `shell=True` ?

.. code:: python

  out = subprocess.run(['/bin/bash', '-c', cmd], stdout=subprocess.PIPE)
  str(out.stdout, 'utf8')
  # '  Number of xml files: 4\n'

argparse lib
-------------

Throughout your integration, you'll have to face it one way or another.

The argparse lib allows the use of command line arguments for a script.
For instance, with the following prog.py:

.. code:: python

  import argparse
  parser = argparse.ArgumentParser()
  parser.add_argument("square", help="display a square of a given number",
                      type=int)
  args = parser.parse_args()
  print(args.square**2)

We can call it from command line and enter a square argument "4" ::

  $ python3 prog.py 4
  16

For more details, follow tutorials below:

- https://docs.python.org/3/howto/argparse.html
- https://docs.python.org/3/library/argparse.html

Use itertools
---------------
.. code:: python

  >>> from itertools import groupby
  >>> [(k, list(g)) for k, g in groupby('AAAACCCDDDGGGCC')]
  [('A', ['A', 'A', 'A', 'A']),
   ('C', ['C', 'C', 'C']),
   ('D', ['D', 'D', 'D']),
   ('G', ['G', 'G', 'G']),
   ('C', ['C', 'C'])]

  >>> from itertools import zip_longest
  >>> list(zip_longest('ABCD', 'ABC'))
  [('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', None)]

  >>> list(zip_longest('ABCD', 'ABC', fillvalue='ZZZ'))
  [('A', 'A'), ('B', 'B'), ('C', 'C'), ('D', 'ZZZ')]

  >>> iterable = 'ABCDEFGHIJK'
  >>> iterable
  >>> 'ABCDEFGHIJK'

  >>> list(zip_longest(*[iter(iterable)]*3) )
  [('A', 'B', 'C'), ('D', 'E', 'F'), ('G', 'H', 'I'), ('J', 'K', None)]

  >>> list(zip_longest(*[iter(iterable)]*4) )
  [('A', 'B', 'C', 'D'), ('E', 'F', 'G', 'H'), ('I', 'J', 'K', None)]

Use pathlib
---------------
Avoid to use the old `os.path` to manipulate Filesystem, prefer the new `pathlib`.

UseCase: Explore all files in a folder structur even when some of them have the
same name::

  /tmp/dir/
  ├── folderA
  │   └── img.png
  ├── folderB
  │   └── img.png
  └── folderC
      └── img.png

unix `cp --backup=numbered` do the trick::

  $ find /tmp/dir -name '*png' |while read i
  do
    cp --backup=numbered $i /tmp/out
  done
  $
  $ tree /tmp/out
  /tmp/out/
  ├── img.png
  ├── img.png.~1~
  └── img.png.~2~

To see this files as images with an image browser like `geeqie` we need to have
all file ending by `.png`::

.. code:: bash

  $ cat swap.py
  # :w|!clear; python swap.py /tmp/out/img.jpg.~1~

.. code:: python

  from sys import argv
  from pathlib import Path

  source = Path(argv[1])
  ext, bkp = source.suffixes
  dest = source.name.replace('{ext}{bkp}', '')
  dest = Path(f'{new_name}_{bkp}{ext}')
  source.rename(dest)
  print(f'source: {source} renamed: {dest}')

now all ends by `.png`::

  $ find . -name '*~'|while read i;do python swap.py $i ;done
  $ tree
  .
  ├── img.~1~.png
  ├── img.~2~.png
  ├── img.png
  └── swap.py
  $


Exercise
==========

Create a new python script in the Python_toolkit directory of your training
repository.

- Create a list of dicts with each dict containing the first and last name of
  each fox as well as the brand of their phone.

.. foxes = [{'prenom': 'Gilles', 'nom': 'Cuyaubère', 'tel': 'Honor'},
..          {'prenom': 'Jeanne', 'nom': 'Chaudanson', 'tel': 'Apple'},
..          {'prenom': 'Baptiste', 'nom': 'Bonnot', 'tel': 'OnePlus'}]

- Print("Gilles' phone is a _________" (fill in the blank with the value from
  your list of dict, of course)).

.. >>> gilles = [x for x in foxes if x['prenom'] == 'Gilles'][0]
.. >>> print(f"Gilles' phone is a {gilles['tel']}")
.. Gilles' phone is a Honor

- Make it so that every 'Apple' phone is replaced by 'Orange'.

.. for fox in foxes:
..     if fox['tel'] == 'Apple':
..         fox['tel'] = 'Orange'

- Write a function that returns the surnames in an alphabetical order.

.. def sort_names(dico):
..     names = [x['nom'] for x in dico]
..     sorted_names = sortedlist(names)
..     return sorted_names

Annexes
========

`.format` String templating
-----------------------------
.. code:: python

  # Using list as argument
  >>> print("Op: {} minus {} is {}".format(33, 13, 20))
  Op: 33 minus 13 is 20

.. code:: python

 # If the order matters use {number} - NOT RECOMMENDED
  >>> print("Op: {2} minus {1} is {0}".format(20, 13, 33))
  Op: 33 minus 13 is 20

  # Using list as argument (do not forget start with *list)
  >>> list_values = [20, 13, 33]
  >>> print("Op: {2} minus {1} is {0}".format(*list_values))
  Op: 33 minus 13 is 20

  # Using dict as argument (do not forget starts with **dict) - RECOMMENDED
  >>> dict_values = dict(argB=13, argA=33, res=20, other_stuff=17)
  >>> dict_values
  {'argA': 33, 'argB': 13, 'other_stuff': 17, 'res': 20}
  >>> print("Op: {argA} minus {argB} is {res}".format(**dict_values))
  Op: 33 minus 13 is 20

  >>> d['corp'] = 'Zettafox'; print(d)
  {'age': 33, 'corp': 'Zettafox', 'name': 'Luis'}
  >>> msg = ("Hello Mr {name}\n"
             "we know you work at {corp} in Paris")

  >>> print(msg.format(**d))
  Hello Mr Luis
      we know you work at Zettafox in Paris

ref: https://docs.python.org/3.8/library/string.html#formatstrings

Very Old string formatting
---------------------------
- The `%` operator (modulo) can also be used for string formatting.
- Given 'string' % values, instances of % in string are replaced with zero or
  more elements of values.
- This operation is commonly known as string interpolation. For example:

.. code:: python

  import math
  print('The value of pi is approximately %5.3f.' % math.pi)
  # The value of pi is approximately 3.142.


Bibliography
-------------
- https://realpython.com/python-data-structures/#typessimplenamespace-fancy-attribute-access
