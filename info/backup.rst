How to backup (+ some ssh)
==========================

1. Get ip
   ip addr
   (look to the one next to the "w" if you're on wifi)

2. Install openssh-serveur
   apt-get install openssh-server

3. Get nas's ip
   web : go to mafreebox.freebox.fr > DHCP
   192.168.1.53

4. Check access to nas as vent
   web : go to http://192.168.1.53:5000 as vent
   with keys in .perso.txt
   check properties of home_vent (in the nas) : /volume1/home_vent

5. Test rsync::

     $ touch /tmp/toto.rst
     $ rsync -aP /tmp/toto.rst 192.168.1.53:/volume1/home_vent/
     vent@192.168.1.53's password: <= pasword from .perso.txt
     sending incremental file list
     $
6. Create tarball and send it in the nas::

     $ cd /home/vent
     $ tar cvf vent_20220224.tar .
     $ rsync -aP vent_20220224.tar 192.168.1.53:/volume1/home_vent/
     ...
     $





SSH
---

1. Get someone to access your computer
He enters ssh username@ip
2. ssh vent@192.168.3.9
Use of asymetrical cryptography



