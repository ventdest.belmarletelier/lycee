Introduction
============
Vers la fin du IIIe après J.C. l'Empire Romain traverse une période de crise :

- Incursions et raids de peuples barbares
- Invasions Sassanides (232-> 259)

.. .. .. note::

    Empereur Valérien capturé

- Tensions internes (multiples assassinats, instabilité croissante)

.. .. note::

    Assassinat d'Alexandre, importance croissante de l'armée,
    Empereurs-soldats (Maximin, Aurélien)

- Sécessions 

  - Postumus et l'Empire des Gaules en 260
  - Zénobie et l'Empire de Palmyre en 271 

Problématique
-------------
Nous allons étudier comment la figure de Zénobie, femme puissante et
ambitieuse, nous est-elle parvenue au travers des siècles et des cultures.

Zénobie et son histoire
=======================

Origines
--------
Suposément née en 240 à Palmyre, elle serait d'après elle une descendante de
Cléopatre, et même de Didon.  Elle est la fille du gouverneur romain de
Palmyre, Zabaii Ben Selim, "Julius Aurelius Zenobe". 

.. .. note::
    
    Ce qui veut dire que ses ancêtres paternels avaient acquéri la citoyenneté
    romaine, probablement sous Marc-Aurèle, entre 161 et 180

Zénobie est décrite par les historiens du VIe siècle comme une femme
particulièrement belle, plus que Cléopâtre, mais surtout douée d'une grande
vertu et d'une grande culture, elle s'entoure de grands philosophes et
s'intéresse à l'histoire et aux langues grecque et égyptienne.
On dit d'elle qu'elle était "prudens" dans ses décisions et "constans" dans ses
plans. 

Elle se marie en 255 à Odénath et le fait nommer gouverneur, ils ont ensemble
un fils, Wahballat.  En 252, l'Empire Sassanide défait les légions romaines en
garnison dans le Levant et conquiert la région, mais Odénath, en 262 et 263 bat
les Sassanides et récupère cette région, puis s'auto-proclame "Roi des Rois".
Cependant il meurt en 267 dans des circonstances mystérieuses.

Zénobie à Palmyre
-----------------
Son mari mort, Zénobie proclame son fils, Wahballat, gouverneur de Palmyre et
"Roi des Rois" et assure la régence de son règne. En 268-269 elle envahit
l'Égypte et en 271 fait sécession avec l'Empire, elle devient alors régente de
l'Empire de Palmyre et de facto, "Reine de Palmyre".

Empire de Palmyre, son expansion et sa fin
------------------------------------------
L'Empire Romain, dirigé par l'Empereur-soldat Aurélien ne peut réagir
immédiatement, aux prises avec un autre Empire rebelle, l'Empire des Gaules de
Marius, Zénobie entreprend donc l'expansion de son empire. Elle parvient en
moins d'un an à conquérir l'Anatolie.

.. .. image:: /src/images/Zenobie_map_01.png

En controlant l'Égypte et tout le Proche-Orient, Zénobie menace gravement Rome;
Aurélien décide donc en 272 de reconquérir Palmyre et de soumettre Zénobie,
l'écart des forces est trop grand et les troupes romaines trop rapides :
l'Empire de Zénobie tombe en moins d'un an.  Zénobie tente de s'échapper chez
les Sassanides mais est capturée par les troupes d'Aurélien et emprisonnée.  On
pert alors la trace de Zénobie mais il y a différentes versions sur la fin de
Zénobie : 


- Elle serait morte de faim pendant une grève de la faim 
- Elle aurait été exécutée par les Romains 
- Aurélien, impréssionné par sa vertu et son éducation, lui aurait permit de
  finir sa vie paisiblement en Italie dans une domus. 

Traces de Zénobie dans la Rome antique
=========================================

L'Histoire Auguste
------------------

DIVUS AURELIANUS FLAVI VOPISCI SYRACUSII  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Texte XXVII :

.. pull-quote:: 

   Hac epistula accepta Zenobia superbius insolentiusque
   rescripsit quam eius quoque fortuna poscebat, credo ad terrorem. Nam eius
   quoque epistulae exemplum indidi: 
   
   "Zenobia regina orientis Aureliano Augusto. Nemo adhuc praeter te hoc, quod
   poscis, litteris petit. [...]"

Traduction (de É. Taillefert) :

Zénobie fit à cette lettre une réponse plus fière, plus insolente, 
que ne le comportait l’état de ses affaires, sans doute pour effrayer son ennemi. 
Voici sa lettre : 

  "Zénobie, reine d’Orient, à Aurélien Auguste. Personne, avant toi, n’avait fait par écrit une telle demande. [...]"

- Fière
- Insolente
- Diplomate 


.. .. note::

    http://www.thelatinlibrary.com/sha/aurel.shtml
    
Pièces de monnaie
-----------------

.. figure:: /src/images/Zenobie_monnaie.jpg

   Nous avons ici un vague aperçu du profil de la Reine palmyréenne


Zénobie du XVIIe siècle à nos jours
===================================

Dans les oeuvres classiques
---------------------------

.. figure:: /src/images/Zenobie_tiepolo_2.jpg


   Ici nous voyons une représentation d'une scène précédant une bataille par Tiepolo.  
   Nous remarquons que Zénobie adopte une position dominante et outrepasse les
   codes sociaux qui enchaînent les femmes de cette époque.

.. .. figure:: /src/images/Herbert_Schmalz_Zenobia.jpg

   Là, H.Schmalz peint le "Dernier regard sur Palmyre",  cité perdue de Zénobie,
   capturée par les Romains.


En France
---------

- Durant la deuxième moitié du XVIIe siècle, un dramaturge français, Jean
  Magnon, écrivit une tragédie : *Zénobie, Reyne de Palmire* où il prônait
  l'égalité entre la femme et l'homme.

- En 1996, l'auteur Bernard Simiot publia *Moi, Zénobie, Reine de Palmyre*, un
  roman historique où il raconte l'histoire de Zénobie et de l'Empire de
  Palmyre. Il porte également un regard très positif sur le caractère et les
  actions de Zénobie durant cette aventure relativement courte.
  
En Syrie
--------

.. .. figure:: /src/images/george_baylouni.png

   Ici, l'artiste syrien George Baylouni, au travers de la représentation d'une
   Zénobie ensanglantée et dont une seule partie du visage est visible, cherche
   à nous montrer la souffrance du peuple syrien. Zénobie est ici l'allégorie
   de la Syrie, meurtrie et divisée.
   
.. figure:: /src/images/palmyre_avant.png

  Au centre nous voyons le théâtre romain, à l'arrière plan à droite, se
  trouvent les ruines de la ville. Enfin à l'extrême droite se trouve le
  Tétrakionon.

.. .. .. note::

   lien vue 3D du Tétrakionon : https://sketchfab.com/3d-models/tetrakionion-palmyra-b96f9b99036d485e988887cfd26e88e6

  
.. .. figure:: /src/images/palmyre_apres.jpg

  En 2015, certains monuments du site de Palmyre, notamment ceux qui
  représentaient les dieux romains ou la reine Zénobie, furent la cible de
  nombreuses destructions volontaires, organisées par les troupes de Daech.

Conclusion
==========

Pour ses contemporains, Zénobie, bien qu'elle soit
intelligente, stratège et
qu'elle sait s'entourer de personnes compétentes, ne reste avant tout qu'une
"mulier", ainsi, elle ne peut pas endosser le rôle de femme-guerrière.
Cependant, aujourd'hui, elle est justement le symbole de la lutte féministe
dans le monde, et principalement, dans le Proche-Orient. Elle représente la
possibilité pour les femmes de conquérir le pouvoir par leurs propres moyens.
