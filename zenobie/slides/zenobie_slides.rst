:css: .my.css

.. footer::

   Luis Belmar Letelier -- Zhou

Zénobie
========

Le règne rebelle et court d’une femme antique
---------------------------------------------

----

Introduction
============
Vers la fin du IIIe après J.C. l'Empire Romain traverse une période de crise :

- Incursions et raids de peuples barbares
- Invasions Sassanides (232-> 259)
- Tensions internes (multiples assassinats, instabilité croissante)
- Sécessions

  - Postumus et l'Empire des Gaules en 260
  - Zénobie et l'Empire de Palmyre en 271

Problématique
-------------
Nous allons étudier comment la figure de Zénobie, femme puissante et ambitieuse, nous
est-elle parvenue au travers des siècles et des cultures.

-----

Zénobie et son histoire 1/4
============================

Origines: Descendante de Cléopâtre ?
------------------------------------
- Née en 240 à Palmyre, dans la province de Syrie
- Elle serait une descendante de Cléopâtre et même de Didon
- Elle est la fille du gouverneur de la cité, Zabaii Ben Selim, "Julius Aurelius Zenobe"

Grande vertu et grande culture
-------------------------------
- Historiens VIe siècle

  - femme + belle que Cléopâtre 
  - grande vertu

- Éprouve un goût pour la culture et les langues
  
  - Elle s'entoure de grands philosophes (comme Longin)
  - Elle connaît les langues latine, araméenne, grecque et égyptienne
  - Elle est "prudens" dans ses décisions et "constans" dans ses plans

----

Zénobie et son histoire 2/4
============================

Odénath et la crise du IIIème siècle
------------------------------------
- En 255, elle se marie à Odénath -> gouverneur ==> un fils, Wahballat
- En 252, invasion de l'Empire Sassanide qui s'empare de la Syrie et du Levant
  
  - En 262 et 263 Odénath défait les Sassanides et récupère les terres romaines

- Odénath se proclame "Roi des Rois"
- Il meurt en 267 dans des circonstances mystérieuses

-----

Zénobie et son histoire 3/4
============================

Accession au pouvoir
---------------------
- Zénobie assure la régence du titre de son fils, Wahballat
  
  - En 268-269 elle reconquiert l'Egypte au nom de Rome 

- En 271, elle fait sécession avec l'Empire Romain
  
  - Devient *de facto* "Reine de Palmyre"

L'Empire de Palmyre, son expansion ...
------------------------------------------
- L'Empire Romain ne peut réagir immédiatement
- Zénobie, parvient à conquérir la quasi-totalité de l'Anatolie en
  moins d'un an.

.. image:: src/images/Zenobie_map_01.png

-----

Zénobie et son histoire 4/4
============================

... et sa fin
-----------------
- Contrôle Palmyre + Egypte + Anatolie ==> Grave menace pour Rome

  - Crise alimentaire
  - Crise économique

- L'Empereur-soldat Aurélien attaque l'Empire de Palmyre en 272

  - Écart des forces trop important
  - Palmyre tombe en un an
  - Zénobie est capturée

- Différentes hypothèses sur la mort de Zénobie

  - Elle serait morte de faim
  - Elle aurait été exécutée par les Romains
  - Aurélien, impressionné par sa vertu et son éducation, l'aurait épargnée

----

:data-scale: 0.1
:data-rotate: 90

Traces de Zénobie dans la Rome antique
======================================

L'Histoire Auguste
------------------

DIVUS AURELIANUS FLAVI VOPISCI SYRACUSII - XXVII
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. pull-quote::

   Hac epistula accepta Zenobia superbius insolentiusque
   rescripsit quam eius quoque fortuna poscebat, credo ad terrorem. Nam eius
   quoque epistulae exemplum indidi:

     "Zenobia regina orientis Aureliano Augusto. Nemo adhuc praeter te hoc, quod poscis,
     litteris petit. [...]"

Traduction (de É. Taillefert) :

.. pull-quote::

   Zénobie fit à cette lettre une réponse plus fière, plus insolente, que ne le
   comportait l’état de ses affaires, sans doute pour effrayer son ennemi.  Voici sa
   lettre :

     "Zénobie, reine d’Orient, à Aurélien Auguste. Personne, avant toi, n’avait fait par
     écrit une telle demande. [...]"

-----

:data-rotate: 90

Pièces de monnaie
=================

Numismatique
------------

.. figure:: src/images/Zenobie_monnaie.jpg

   Nous avons ici un vague aperçu du profil de la Reine palmyréenne

-----

Zénobie du XVIIe siècle à nos jours
===================================

- Dans les oeuvres classiques
- En France
- En Syrie

-----

Dans les oeuvres classiques
---------------------------

.. figure:: src/images/Herbert_Schmalz_Zenobia.jpg
   :width: 400px

   Ici, H.Schmalz peint le *Dernier regard sur Palmyre*, cité perdue de Zénobie,
   capturée par les Romains.  

.. figure:: src/images/Zenobie_tiepolo_2.jpg
   :width: 400px

   *Zénobie harangue ses soldats*, de Tiepolo. Nous remarquons que Zénobie adopte une
   position dominante vis à vis de ses soldats, qui sont tous des hommes et outrepasse
   les codes sociaux qui enchaînent les femmes de cette époque.

-----

En France
---------

+ Deuxième moitié du XVIIe siècle, un dramaturge français, Jean Magnon:
  *Zénobie, Reyne de Palmire*. Il prône dans cette pièce l'égalité entre l'homme et la femme.

- En 1996, Bernard Simiot publie *Moi, Zénobie, Reine de Palmyre*.
  Roman historique sur la vie de Zénobie. Il porte également un regard très positif sur
  le caractère non-conventionnel de Zénobie. 
  
-----

En Syrie 1/3
=============

Les ruines de la cité antique
------------------------------

.. figure:: src/images/palmyre_avant.png
   :width: 800px

   Au centre,le théâtre romain; à l'arrière plan à droite, les ruines de la ville et à
   l'extrême droite, le Tétrakionon.

-----

En Syrie 2/3
=============

l'horreur du fanatisme
----------------------

.. .. figure:: src/images/palmyre_apres.jpg
..    :width: 400px

   En 2015, certains monuments du site de Palmyre, notamment ceux qui représentaient les dieux romains ou la reine Zénobie, furent détruits par DAECH. 

-----

En Syrie 3/3
=============

Allégorie de la Syrie, meurtrie et divisée
------------------------------------------

.. image:: src/images/george_baylouni.png
   :width: 600px

-----

Conclusion
==========

- Pour ses contemporains:

  - Intelligente et stratège
  - S'entoure de personnes compétentes
  - Ne reste avant tout qu'une "mulier", ainsi, elle ne peut pas endosser le rôle de femme-guerrière.

- Aujourd'hui:

  - Elle est le symbole de la lutte féministe et populaire dans le Proche-Orient comme dans le monde
  - Possibilité pour les femmes de conquérir le pouvoir par leurs propres moyens.

