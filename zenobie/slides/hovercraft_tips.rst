:css: .my.css

.. footer:: 

   Luis Belmar Letelier -- Zhou

----

=======
ZENOBIE
=======

Un truc fun en Latin
====================

----

Intro
=====
Vers la fin du IIIe après J.C. l'Empire Romain traverse une période très
difficile... Zenobie => Palmyre ... patati patata

Problématique
-------------
Nous allons étudier comment la figure de Zénobie, femme
puissante et ambitieuse, nous est-elle parvenue au travers des siècles et des
cultures.

----

.. :data-rotate-z: 90

I Zenobie et son histoire
=========================
patata

Zenobie à Palmyre
-----------------
xxx

Empire de Palmyre et son expansion
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
xxx

.. image:: ../src/images/Zenobie_map_01.png
   :width: 400px

----

:id: traces_antiques

II Traces de Zénobie dans la Rome antique 1/2
===============================================
patata

L'Histoire Auguste
------------------

DIVUS AURELIANUS FLAVI VOPISCI SYRACUSII  
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Voici 6 paragraphes du chapitre sur Divus Aurelianus où il est fait mention de
Zenobie : (XXV, XXVI, XXVII, XXVIII, XXXII, XXXIII)

XXVII

1 Hac epistula accepta Zenobia superbius insolentiusque rescripsit quam eius quoque
fortuna poscebat, credo ad terrorem. Nam eius quoque epistulae exemplum indidi:

2 "Zenobia regina orientis Aureliano Augusto. Nemo adhuc praeter te hoc, quod poscis,
litteris petit. Virtute faciendum est quidquid in rebus bellicis est gerendum.  

3 Deditionem meam petis, quasi nescias Cleopatram reginam perire maluisse quam in
qualibet vivere dignitate. 

4 Nobis Persarum auxillia non desunt, quae iam speramus, pro nobis sunt Saraceni, pro
nobis Armenii.

5 Latrones Syri exercitum tuum, Aureliane, vicerunt.  Quid? Si igitur illa venerit
manus, quae undique speratur, pones profecto supercilium, quo nunc mihi deditionem,
quasi omnifariam victor, imperas." 

6 Hanc epistulam Nicomachus se transtulisse in Graecum ex lingua Syrorum dicit ab ipsa
Zenobia dictatam. Nam illa superior Aureliani Graeca missa est. 

.. class:: substep

  Voyons plus en datail **XXVII - 1**

.. http://www.thelatinlibrary.com/sha/aurel.shtml

----

II Traces de Zénobie dans la Rome antique 2/2
==============================================

DIVUS AURELIANUS FLAVI VOPISCI SYRACUSII -- XXVII
---------------------------------------------------

Latin
~~~~~~
1 Hac epistula accepta Zenobia superbius insolentiusque rescripsit quam eius quoque
fortuna poscebat, credo ad terrorem. Nam eius quoque epistulae exemplum indidi:

Traduction
~~~~~~~~~~~~
latency on communications and **Synchrony** matters !) - => some nodes can be tempted to
cheat: **Byzantine nodes** or just be

----

III Zenobie aujourd'hui
=======================

Dans les oeuvres classiques
---------------------------

.. figure:: ../src/images/oeuvres_classiques.png
   :width: 600px

   Louvre Nom des tableaux

balbalabla

----

Conclusion
==========
xxx

.. https://hovercraft.readthedocs.io/en/latest/presentations.html#impress-js-fields

.. .. class:: substep
.. :data-scale: 0.1
.. :data-rotate-z: 90
