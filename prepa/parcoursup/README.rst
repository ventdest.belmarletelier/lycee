PROJET DE FORMATION MOTIVÉ MPII
================================

==> parler de git, rst, et python

PROJET DE FORMATION MOTIVÉ MPSI
================================


MES ACTIVITÉS
=============

Mes expériences d'encadrement ou d'animation
--------------------------------------------
Décrivez ici vos expériences d'encadrement ou d'animation (1500 caractères max.)

Mon engagement citoyen
----------------------
Décrivez ici votre engagement citoyen ou bénévole dans une association, dans votre établissement ou un autre cadre (1500 caractères max.)

Mon expérience professionnelle
------------------------------
Décrivez ici vos expériences professionnelles ou les stages que vous avez effectués 
(1500 caractes max.).


Ouverture au monde
------------------
(pratiques sportives et culturelles, parcours spécifiques)

Décrivez ici vos pratiques sportives et culturelles
---------------------------------------------------
et/ou les parcours spécifiques suivis (exemple : cordées de la réussite,
parcours d’excellence). Il peut également s’agir de la pratique d’une langue
étrangère non étudiée au lycée, de séjours à l'étranger ou d’un intérêt
particulier pour une culture étrangère. (1500 caractères max.)

J'ai appris à lire et à écrire quatre langues étrangères, l'anglais (préparation au TOEIC), l'espagnol (Brevet international mention très bien, équivalent C1), le russe (certification B2) et le chinois mandarin (certification HSK3 et préparation du HSK4) 
Je porte également un grand intérêt pour les cultures hispanique, slave et asiatique. 
