Le goût de l'aprentissage et la curiosité sont les deux passions qui ont guidé mon parcours pendant le lycée et le collège. C'est pourquoi, en cas de difficultés, j'ai toujours su me relever en fournissant un travail conséquent et adapté aux obstacles.

Je suis ferru de tout contenu portant, sur la physique et les mathématiques, cette passion me permet de progresser et de fournir une quantité de travail importante dans mes études.
Durant mon parcours scolaire, j'ai pu diversifier mes centres d'intérêt, notamment en effectuant le parcours d'Apprenti-chercheur, organisé par l'ENS, ainsi qu'en étudiant la guitare classique ainsi que le solfège pendant 5 ans au conservatoire, mais également en apprenant plusieurs langues, en effet, j'ai appris à parler, lire et écrire en cinq langues différentes, le français, l'anglais, l'espagnol (Brevet National Espagnol, équivalent C1), le russe (B2) et le chinois (HSK3, préparation du HSK4).

Animé par le sentiment de l'entraide et du devoir, j'ai rejoint les Jeunes Sapeurs Pompiers de Paris, un organisme de jeunesse dépendant de la Brigade des Sapeurs Pompiers de Paris. Cette formation, à l'issue delaquelle j'ai obtenu le Brevet National de Jeune Sapeur Pompier, m'a inculqué une discipline et un mental de fer, que j'ai su capitaliser dans mes études.

Mon ambition est d'intégrer une école d'ingénieurs afin de faire de la recherche dans le domaine de l'énergie.

Je dois donc être prêt et préparé, c'est pourquoi je souhaiterai être accepté dans votre établissement. En effet, les retours d'anciens élèves, les excellents résultats aux concours ainsi que le niveau particulièrement élevé d'enseignement des professeurs de votre établissement me portent à croire que c'est dans celui-ci que j'aurais le plus de chances d'accéder à l'excellence.

J'espère que ma candidature aura retenu votre attention.
Veuillez agréer, Madame, Monsieur, à mes salutations les plus distinguées.

Luis Belmar Letelier -- Zhou
