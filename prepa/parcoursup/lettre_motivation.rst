Je souhaite intégrer une classe PCSI au sein de votre établissement.

Le lycée Charlemagne est en effet réputé pour le niveau de formation qu'il offre et pour sa capacité à préparer aux plus grandes écoles. De plus je réside à seulement quelques minutes de ce lycée.

Le goût de l'apprentissage et la curiosité sont les deux passions qui ont guidé mon parcours pendant le collège et le lycée. C'est pourquoi, en cas de difficultés, j'ai toujours su me relever en adaptant mon travail aux obstacles.

Je suis féru de tout contenu portant sur la physique et les mathématiques, cette passion me permet de progresser et de fournir une quantité de travail importante dans mes études.

En dehors des études j'ai participé aux Olympiades de Mathématiques, de Chimie, et j'ai suivi le parcours Apprenti-chercheur de l'ENS. Je maîtrise 5 langues: le français, l'anglais, l'espagnol (C1), le russe (B2) et le chinois (HSK3, préparation du HSK4). Je suis breveté Jeune Sapeur Pompier de Paris depuis octobre 2022. 
J'ai installé mon premier Linux en 6ème, j'utilise git pour versionner une partie de mon travail, que j'expose sur un modeste site sur un Raspberry Pi que j'ai mis en ligne. La programation en Python, en bash et en HTML me passionne.

Je suis sûr que la saine émulation créée par votre prestigieux établissement me permettra de donner le meilleur de moi-même. 

Je vous remercie de l'intérêt porté à ma candidature.
