Create a venv::

  vent@pixel:~$ python3 -m venv ~/venv
  vent@pixel:~$ tree ~/venv/bin/
  ~/venv/bin/
  ├── activate
  ├── pip3
  ├── pip3.9
  ├── python -> python3
  ├── ...
  └── python3.9 -> python3
  vent@pixel:~$ 

Activate venv::

  vent@pixel:~$ source ~/venv/bin/activate
  (venv) vent@pixel:~$

