"""
create a complement() function that calculate 2-complement of binary
represented as an array/list (undefined length)
"""
def swap_0_1(n):
    if n == 0:
        n = 1
    else:
        n = 0
    return n

"""
usefull tips

In [23]: ll = [0, 1, 2]
In [24]: for ind in range(len(ll)):
    ...:     print(ll[len(ll)-1-ind])
2
1
0


In [36]: for i in sorted(ll, reverse=True):
    ...:     print(i)
    ...:
2
1
0

"""

def complement2(bin_array):
    """
    invert 0 and 1 on each digit, and add 1
    e.g. bin_array = [0, 1, 0]
    [1, 0, 1]                    # 5
    [1, 0, 1] + 1 -> [1, 1, 0]   # 6

    1011 + 1
       ^
       --> 0   out = 1010 retenue = 1
    1010
      ^
      --> 1 + retenu = 2> 1 =>out= 0 retenue = 1
    1000
     ^
     --> 0 + retenue = 1 = 1<=1 out = 1 =>  retenue=0
    """
    print('bin_array input:', bin_array)
    res = []
    for n in bin_array:
        if n == 0:
            res.append(1)
        else:
            res.append(0)
    # print('res:', res, sorted(res, reverse=True))

    # res[-1] = res[-1] + 1
    # for i in sorted(res, reverse=True):
    #     print(res[i])
    #     print(sorted(res, reverse=True))
    #     if res[i] > 1:
    #         res[i], res[i+1] = 0, 1 #problem: last element out of range
    #       return res
    # petit caillous blanc (next)

    rres= res.copy()
    rres.reverse()
    print('rres:', rres)

    comp2 = []
    retenue = 0
    for i in rres:
        retenue = 0
        print('i:', i)
        if i == 1:
            retenue = 1
            rres[i] = 0
            rres[i] = rres[i]+retenue
            comp2.append(res[i])
        else:
            retenue = 0
            rres[i] = rres[i]+retenue
            comp2.append(res[i])
    print('comp2', comp2)

# complement2([0, 1, 0])
# complement2([1, 0, 1, 1])





















#25/11
print('== start')
def inv(n):
    """Hypothèse: n is list representing a 8 bits binary"""
    inv = []
    for j in n:
        if n[j] == 0:
            inv.append(1)
        else:
            inv.append(0)
    return inv

print(inv([0, 0, 0, 0, 0, 1, 1, 0]))

"""def plusone(n):
"""
