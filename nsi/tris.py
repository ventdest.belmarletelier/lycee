#
# Insertion sort
#
def insertion_sort(liste):
    """
    i=0, j=0, nada
    i=1, j=1,   rien
    i=1, j=0, nada
    i=2, j=2, ll[1]=6, ll[2]=2 While True
        ll[1]=2, ll[2]=6  swap
        ll = [4, 2, 6, 9, 3, 1] # FixMe
    ...
    """
    for i in range(0, len(liste)):
        j = i
        while j>0 and liste[j-1] > liste[j]:
            liste[j-1], liste[j] = liste[j], liste[j-1]
            j = j - 1
    return liste

ll = [4, 6, 2, 9, 3, 1]
print(f'to sort: {ll}')
print(f'sorted: {insertion_sort(ll)}')



#
# Selection sort
#
def mini(liste, debut:int):
    """
    len(liste) = 100 -> 1s
    len(liste) = 100 x 10 -> 1s x 10 ?
    len(liste) = 100 x 2 -> 1s x 2^2 ?
    len(liste) = 100 x 100 -> 1s x 100^2            complexité N^2
    len(liste) = 100 x 100 -> 1s x 100*log(100)     complexité N*log(N)
    len(liste) = 100 x 100 -> 1s x 100              complexité N
    """
    index_mini = debut
    for i in range(debut+1, len(liste)):  # N
        if liste[index_mini] > liste[i]:
            index_mini = i
    return index_mini

def selection_sort(liste):
    for j in range(0, len(liste)):        # N
        index_mini = mini(liste, j)       # N
        # tmp = liste[j]
        # liste[j] = liste[index_mini]
        # liste[index_mini] = tmp
        liste[j], liste[index_mini] = liste[index_mini], liste[j]
    return liste

ll = [4, 6, 2, 9, 3, 1]
print(f'to sort: {ll}')
print(f'sorted: {selection_sort(ll)}')
