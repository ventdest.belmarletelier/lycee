def mini(liste, debut:int):
    """
    len(liste) = 100 -> 1s
    len(liste) = 100 x 10 -> 1s x 10 ?
    len(liste) = 100 x 2 -> 1s x 2^2 ?
    len(liste) = 100 x 100 -> 1s x 100^2            complexité N^2
    len(liste) = 100 x 100 -> 1s x 100*log(100)     complexité N*log(N)
    len(liste) = 100 x 100 -> 1s x 100              complexité N
    """
    index_mini = debut
    for i in range(debut+1, len(liste)):  # N
        if liste[index_mini] > liste[i]:
            index_mini = i
    return index_mini

def selection_sort(liste):
    for j in range(0, len(liste)):        # N
        index_mini = mini(liste, j)       # N
        # tmp = liste[j]
        # liste[j] = liste[index_mini]
        # liste[index_mini] = tmp
        liste[j], liste[index_mini] = liste[index_mini], liste[j]
    return liste


ll = [4, 6, 2, 9, 3, 1]
print(f'to sort: {ll}')
print(f'sorted: {selection_sort(ll)}')
