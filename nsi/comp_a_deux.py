# 6    : '00000110'
# 6inv : '11111001'
# +1   : '00000001'
# -6   : '11111010'

# check that this is -6
# -6   : '00000101'
# +1   : '00000001'
# 6    : '00000110'  dec => 0 + 2 + 4 = 6


def comp_to_1(binary:str) -> str:
    res = ['0' if i=='1' else '1' for i in binary]
    # print('comp_to_1:', ''.join(res))
    return ''.join(res)


def add_one(binary):
    output = []
    rev = ''.join(reversed(binary))
    for idx, digit in enumerate(rev):
        # print('idx, digit:', idx, digit)
        if idx == 0:
            res = int(digit) + 1
            if res == 2:
                output.append('0')
                retenue = 1
            else:
                output.append('1')
                retenue = 0
        else:
            res = int(digit) + retenue
            if res == 2:
                output.append('0')
                retenue = 1
            else:
                output.append(str(res))
                retenue = 0
    return ''.join(reversed(output))


def inverse_comp_a_deux(positive):
    cto1 = comp_to_1(positive)
    res = add_one(cto1)
    return res


# test
print("== input +6: \n 00000110")
assert '11111001' == comp_to_1('00000110')

print('== add_one: \n',   add_one('11111001'))
assert '11111010' == add_one('11111001')

print('== inverse_comp_a_deux: \n',   inverse_comp_a_deux('00000110'))
assert '11111010' == inverse_comp_a_deux('00000110')

