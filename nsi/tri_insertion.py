def insertion_sort(liste):
    """
    i=0, j=0, j not > 0                 no_swap

    i=1, j=1, liste[0]=4 < liste[1]=6   no_swap  [4, 6, 2, 9, 3, 1]

    i=2, j=2, liste[1]=6 > liste[2]=2   go_swap  [4, 6, 2, 9, 3, 1]
    i=2, j=2, liste[1]=2,  liste[2]=6 j=1  swap  [4, 2, 6, 9, 3, 1]
    i=2, j=1, liste[0]=4 > liste[1]=2   go_swap  [4, 2, 6, 9, 3, 1]
    i=2, j=1, liste[0]=2,  liste[1]=4 j=0no_swap [2, 4, 6, 9, 3, 1]

    i=3, j=3, liste[2]=6 < liste[3]=9   no_swap  [2, 4, 6, 9, 3, 1]

    i=4, j=4, liste[3]=9 > liste[4]=3   go_swap  [2, 4, 6, 9, 3, 1]
    i=4, j=4, liste[3]=3,  liste[4]=9 j=3  swap  [2, 4, 6, 3, 9, 1]
    i=4, j=3, liste[2]=6 > liste[3]=3   go_swap  [2, 4, 6, 3, 9, 1]
    i=4, j=3, liste[2]=3,  liste[3]=6 j=2  swap  [2, 4, 3, 6, 9, 1]
    i=4, j=2, liste[1]=4 > liste[2]=3   go_swap  [2, 4, 3, 6, 9, 1]
    i=4, j=2, liste[1]=3 > liste[2]=4 j=1  swap  [2, 3, 4, 6, 9, 1]
    i=4, j=1, liste[0]=2 < liste[1]=3   no_swap  [2, 3, 4, 6, 9, 1]
    """
    for i in range(0, len(liste)):
        j = i
        while j>0 and liste[j-1] > liste[j]:
            liste[j-1], liste[j] = liste[j], liste[j-1]
            j = j - 1
    return liste


ll = [4, 6, 2, 9, 3, 1]
print(f'to sort: {ll}')
print(f'sorted: {insertion_sort(ll)}')

