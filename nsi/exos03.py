# - Create a list of dicts with each dict containing the first and last name of
#   each fox as well as the brand of their phone.

print('hello')

ld = [{'nom' : "Papa" ,'tel' : "One"}, {'nom' : "Maman" , 'tel' : "Huawei"},
      {'nom': "Ventvent", 'tel' : "Mi"}, {'nom': "Mario", 'tel' : "Nokia"}]
print(ld)


#
# - Print("Gilles' phone is a _________" (fill in the blank with the value from
#   your list of dict, of course)).
#
# - Make it so that every 'Nokia' phone is replaced by 'iPhone'.


def rep(ld):
    print('== in rep')
    for d in ld:
        print(d['nom'], d['tel'])
        if d['tel'] == "Nokia":
            d['tel'] = "iPhone"
        print(f"{d['nom']}'s phone is a {d['tel']}")
    return ld

print('before rep:', ld)
out = rep(ld)
print('after rep:', out)


# - Write a function that returns the surnames in an alphabetical order.

def sort(ld):
    print('== in sort')
    sl = []
    for d in ld:
        sl.append(d['nom'])
        print(sl)
    sl.sort()
    return sl

out = sort(ld)
print(out)
print('simpler:', sorted([d['nom'] for d in ld]))


