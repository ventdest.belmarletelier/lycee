.. zenobie documentation master file, created by
   sphinx-quickstart on Sun Mar 27 19:28:52 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Exposé Zenobie
===============

.. toctree::
   :maxdepth: 3
   :caption: Table des Matieres:

   /src/antiquite.rst
   /src/biographie.rst
   /src/moderne.rst

