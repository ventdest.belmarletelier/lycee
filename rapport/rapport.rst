================
Nom du document
================
.. contents:: 

Requirements
=============
Install pandoc to convert rst to latex::

  $ sudo apt-get install pandoc

Install latex to compile latex in PDF::

  $ sudo apt-get install texlive-latex-base

Install docutils to convert rst into HTML and ODT::

  $ pip install docutils

Mon premier doc rst
====================
- liste à puces
- autre chose

  - sous liste A
  - sous liste B
  - sous liste C

Un code Python
---------------
.. code:: python

  def insertion_sort(liste):
      for i in range(0, len(liste)):
          j = i
          while j>0 and liste[j-1] > liste[j]:
              liste[j-1], liste[j] = liste[j], liste[j-1]
              j = j - 1
      return liste

  ll = [4, 6, 2, 9, 3, 1]
  print(f'to sort: {ll}')
  print(f'sorted: {insertion_sort(ll)}')

Des Math
---------
Prouver par récurrence que pour tout :math:`n` dans :math:`\mathbb{N}^*`:

.. math::

  \sigma_n = 1 + 2 + 3 + ... + n = \sum_{k=1}^{n} k = \dfrac{n(n+1)}{2}

Mes images
----------

.. image:: ./images/cute_winter.jpg
   :width: 400px
