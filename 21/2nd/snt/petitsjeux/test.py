from time import sleep


def dialogueintro():
    msg = (f"Bonjour comment vas-tu ? \n"
            " ( 1. Bien / 2. Mal ) \n")
    rep = input(msg)
    sleep(1)
    if rep == '1':
        print("Cool")
    else:
        print('Ah mince... ')
        sleep(1)
        msg = ("Que s'est t-il passé ?\n"
               " ( 1. J'ai pas bien mangé ce matin /\n  ")
        msg += (" 2. J'ai eu une mauvaise note ) \n")
        rep = input(msg)
        sleep(1)
        if rep == "2":
            msg = ("Aie... Tu veux que je t'aide pour le\n"
                   "prochain examen ? \n ")
            msg += ("( 1. Oui merci beaucoup ! / \n"
                    "   2. Non, t'inquiète, merci... )\n")
            rep = input(msg)
            sleep(1)

dialogueintro()
