# -*- coding: UTF-8 -*-
# pip install wheel; pip install lxml pandas openpyxl

from urllib.request import Request, urlopen
import lxml.html
import pandas as pd
import re
import urllib.request

# Récupérer une page web et sa dom html
URL = 'https://www.boulanger.com/c/ecouteur?m=g'
headers = dict(ua=('Mozilla/5.0 (X11; Linux i686) AppleWebKit/537.17 '
                   '(KHTML, like Gecko) Chrome/24.0.1312.27 Safari/537.17'))
headers['User-Agent'] = headers.pop('ua')
request = Request(URL, headers=headers)
resp_str = urlopen(request).read()
html = lxml.html.fromstring(resp_str)

## Use xpath
titles = [tt.text_content().strip() for tt in
          html.xpath("//a[@class='grid-product-name']/h2")]
prices = [pp.text_content().strip() for pp in
          html.xpath("//p[@class='fix-price']")]
records = zip([re.sub('\s+', ' ', tt) for tt in titles],
              [re.sub('\s+', ' ', pp) for pp in prices])
df = pd.DataFrame.from_records(list(records), columns=['title', 'price'])
print(df.head())

# Save in xlsx
xlsx_file = "out.xlsx"
df.to_excel(xlsx_file)

print(f'\n{xlsx_file} ({df.shape[0]} lines) Saved !')
