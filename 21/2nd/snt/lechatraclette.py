# run with
# :w|!clear; python images.py

import PIL.Image as pil

# open images
img_paysage = pil.open('data/paysage.jpeg')
img_chat = pil.open('data/chat.jpeg')

# final image
mix_size = min(img_paysage.size, img_chat.size)
img_mix = pil.new('RGB', mix_size)
print(mix_size)

largeur, hauteur = mix_size
for w in range(largeur):
    for h in range(hauteur):
        cr, cg, cb = img_chat.getpixel((w, h))
        r, g, b = img_paysage.getpixel((w, h))
        img_mix.putpixel((w,h), (max(r,cr), max(g,cg), max(b, cb)))
img_paysage.show()
img_chat.show()
img_mix.show()
