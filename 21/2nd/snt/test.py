import numpy as np
import matplotlib.pyplot as plt

x = np.linspace(0, 2 * np.pi, 100)
# plt.axis([x[0], x[-1], -1, 1])      # disable autoscaling
pair = True
for point in x:
    pair = not pair
    plt.plot(point, np.sin(2 * point), '*', color='r' if pair else 'b')
    plt.draw()
    plt.pause(0.01)
# plt.clf()
