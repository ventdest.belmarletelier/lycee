# from math import *
from random import random
import numpy as np
import matplotlib.pyplot as plt

#def tirage_1(p):
#   p = random(0, 1)
#   if random == 0:
#       print("c'est une fille")
#   if random == 1:
#       print("c'est un garçon")
#   return


def tirage_2(seuil):
    """
    return 0 si la naissance est > au seuil
    """
    naissance = random()
    if naissance > seuil:
        return 0
    else:
        return 1


def tirage_20(n, p):
    """
    tirage_20(20, 0.512)
    """
    f = 0
    for i in range(n):
        f += tirage_2(p)
        frequence = f/n
    return(frequence)


def tirage_100(N):
    """
    return a N-vector of frequences
    """
    frequence = []
    for loop in range(N) :
        new = tirage_20(20, 0.512)
        frequence.append(new)
    print(frequence)
    return frequence


def tiragegraph_100():
    x = [i for i in range(100)]
    y = tirage_100(100)
    print(y)
    plt.scatter(x, sorted(y))
    plt.show()
    # plt.pause(2)  # with plt.show() picture stay on screen


if __name__ == '__main__':
    out = tirage_2(0.512)
    print(f'tirage_2: {out}')
    tiragegraph_100()

