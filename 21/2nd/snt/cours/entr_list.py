## Entr_py_2

s, l, d = "Bonjour", ["Moi", 15.5, 2021, "Moi"], {"age": 15, "nom": "Moi"}
l
#['Moi', 15.5, 2021]

# list API

print("l.append() ==> OK")
l
#['Moi', 16.5, 2021]
l.append('Paris')
l
#['Moi', 15.5, 2021, 'Paris']

print("l.clear() ==> OK")
l
#['Moi', 16.5, 2021]
l.clear()
l
#[]

print("l.copy() ==> KO")

print("l.count() ==> OK")
l = ['Moi', 15.5, 2021, 'Moi']
l
#['Moi', 15.5, 2021, 'Moi']
l.count('Moi')
#2
l.count(2021)
#1
l.count(2)
#0

print("l.extend() ==> KO")

print("l.index() ==> OK")
l
#['Moi', 15.5, 2021, 'Moi']
l.index('Moi')
#0
l.index(15.5)
#1
l.index(2021)
#2

print("l.insert() ==> OK")
l = ['Moi', 15.5, 2021, 'Moi']
l.insert(1, "maison")
l
#['Moi', 'maison', 15.5, 2021, 'Moi']

print("l.pop() ==> OK")
l = ['Moi', 15.5, 2021, 'Moi']
l.pop()
#'Moi'
l.pop(2)
#2021
l
#['Moi', 15.5]

print("l.remove() ==> OK")
l
#['Moi', 15.5, 2021, 'Moi']
l.remove('Moi')
l
#[15.5, 2021, 'Moi']
l.remove(2021)
l
#[15.5, 'Moi']

print("l.reverse() ==> OK")
l = ['Moi', 15.5, 2021, 'Moi']
l.reverse()
l
#['Moi', 2021, 15.5, 'Moi']

print("l.sort() ==> OK")
l = [2134, 45.66, 34, 45.55]
l
#[2134, 45.66, 34, 45.55]
l.sort()
l
#[34, 45.55, 45.66, 2134]
l = ["trucbidule", "az", "ca", "bo"]
l.sort()
l
#['az', 'bo', 'ca', 'trucbidule']
l = ["az", "ca", 31.55, "bo", 3435, 31.56]
l.sort()
#Traceback (most recent call last):
#  File "<input>", line 1, in <module>
#    l.sort()
#TypeError: '<' not supported between instances of 'float' and 'str'

print('== comprehensive list')
# resultat attendu, a partir de quoi, conditions
ll = ['copy', 'count', 'extend', 'index', 'insert', 'pop', 'remove', 'reverse']

res = []
for s in ll:
    if s[0] in ['c', 'r']:
        res.append((s, s.upper()))
# [('copy', 'COPY'),
#  ('count', 'COUNT'),
#  ('remove', 'REMOVE'),
#  ('reverse', 'REVERSE')]

[(s, s.upper()) for s in ll if s[0] in ['c', 'r']]
# [('copy', 'COPY'),
#  ('count', 'COUNT'),
#  ('remove', 'REMOVE'),
#  ('reverse', 'REVERSE')]


