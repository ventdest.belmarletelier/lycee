# :w|!clear; python3 easter_train.py

## Strings, lists and dictionaries
s, l, d = 'hello', [22, 3.14, 'world'], {'age': 33, 'name': 'Luis'}
l, s, d
# ([22, 3.14, 'world'], 'hello', {'age': 33, 'name': 'Luis'})

## Tuples
t = (22, 3.14, 'hello')
t
# (22, 3.14, 'hello')

print('====== Dictionary')
d = {'age': 33, 'name': 'Luis', 'ville': 'Paris', 'nb': 3}
del(d['nb'])
d
print(d)

d = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 7678}
for k, v in d.items():
    print(f'key: {k}')
    print(f'value: {v}')

# key: Sjoerd
# value: 4127
# key: Jack
# value: 4098
# key: Dcab
# value: 7678

# In [76]: d
d = {'Sjoerd': 4127, 'Jack': 4098, 'Dcab': 7678}
# In [77]: d.items()
# Out[77]: dict_items([('Sjoerd', 4127), ('Jack', 4098), ('Dcab', 7678)])
# In [78]: print(f'=={i:<10}==>')
# ==Dcab      ==>
# In [79]: print(f'==>{j:>10}==')
# ==>      7678==
for i, j in d.items():
    print(f' {i:<10}  ==>  {j:>10}')

#  Sjoerd      ==>        4127
#  Jack        ==>        4098
#  Dcab        ==>        7678

print('====== String API')
print('== capitalize ok')
print('== casefold ko')
s.capitalize() == 'Hello'

print('== count')

# In [17]: s, s.count('l')
# Out[17]: ('hello', 2)
# In [18]: s, s.count('e')
# Out[18]: ('hello', 1)
# In [19]: s, s.count('z')
# Out[19]: ('hello', 0)

In [31]: def plus(x, y=3, *a_list):
    ...:     # default argument should follows non-default argument
    ...:     print(f'We see {len(a_list)} aditionnal arguments: {a_list}')
    ...:     our_ints = [i for i in a_list if isinstance(i, int)]
    ...:     print(f'sum them all: {sum(our_ints)}')
    ...:     print(f'{x} + {y} = {x+y}')
    ...:
    ...:
    ...:

In [32]: plus(7, 14, 'a', 'coucou', 56, 17, 12)
We see 5 aditionnal arguments: ('a', 'coucou', 56, 17, 12)
sum them all: 85
7 + 14 = 21

In [33]: print(__builtins__.sum.__doc__)


def plus(x, y=3, **my_dict):
    print(f'{x} + {y} = {x+y}')
    print(f"Nom: {my_dict['nom']}, Age: {my_dict['age']}, Habite à: {my_dict['ville']}")
    print()
    print(my_dict)
    for k, v in my_dict.items():
        print(f'{k}: {v}')

plus(7, 14, nom="Vent d'est", age=15, ville='Paris')
# Nom: Vent d'est, Age: 15, Habite à: Paris
# {'nom': "Vent d'est", 'age': 15, 'ville': 'Paris'}
# 7 + 14 = 21


In [61]: def plus(x, y=3, **my_dict):
    ...:     print(f'{x} + {y} = {x+y}')
    ...:     print(f"Nom: {my_dict['nom']}, Age: {my_dict['age']}, Habite à: {my_dict['ville']}\n")
    ...:     # print(my_dict)
    ...:     for k, v in my_dict.items():
    ...:         print(f'{k:20} => {v}')
    ...:

plus(7, 14, nom="Vent d'est", age=15, ville='Paris')
# 7 + 14 = 21
# Nom: Vent d'est, Age: 15, Habite à: Paris
#
# nom                  => Vent d'est
# age                  => 15
# ville                => Paris

In [75]: plus(7, 14, **{'nom': "Vent d'est", 'age': 15, 'ville': 'Paris'})
7 + 14 = 21
Nom: Vent d'est, Age: 15, Habite à: Paris

nom                  => Vent d'est
age                  => 15
ville                => Paris

In [76]:




    In [82]:   def my_plus(x, y=2, *optional_list, **optional_dict):
    ...:       "my_plus illustrates a python function"
    ...:       result = []
    ...:       # y has the default value: 2
    ...:       result.append(f'{x} plus {y} is {x+y}')
    ...:
    ...:       # optional_list is ... optional
    ...:       if optional_list:
    ...:           result.append(f'list: len: {len(optional_list)} '
    ...:                         f'items: {optional_list}')
    ...:       # optional_dict is ... optional
    ...:       if optional_dict:
    ...:           msg = (f'dict: len: {len(optional_dict)};  '
    ...:                  f'  keys: {list(optional_dict.keys())};  '
    ...:                  f'  values: {list(optional_dict.values())}')
    ...:           result.append(msg)
    ...:       for line in result:
    ...:           print(line)
    ...:

In [83]: my_plus(64, 12, 'aa', 'zz', town='Shanghai', note='AAA')
64 plus 12 is 76
list: len: 2 items: ('aa', 'zz')
dict: len: 2;    keys: ['town', 'note'];    values: ['Shanghai', 'AAA']

In [84]:

