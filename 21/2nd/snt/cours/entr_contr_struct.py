#-----------
## Entr_py_5
# -----------

# ====================
## CONTROL STRUCTURES
# ====================

s, l, d = "Bonjour", ["Moi", 15.5, 2021, "Moi"], {"age": 15, "nom": "Moi"}
dl = ["z", "o", "e", 3, 7, "Luis", "RR"]
ll = l + list(dl) ; print(ll)
#['Moi', 15.5, 2021, 'Moi', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']


## a = X if cond1 else Y
# =======================

name = "Luis"
name = 'Luis'
long_name = name if len(name) > 5 else False
long_name
#False
long_name = name if len(name) > 2 else False
long_name
#'Luis'

name = "Leopold3" 
long_name = name.upper() if len(name) > 5 else False
long_name
#'LEOPOLD3'


## for Loop
# ==========

ll
#['Moi', 15.5, 2021, 'Moi', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']
len(ll)
#11
for i in ll: 
    print("a", end=".")

## if, elif, elif, ..., else

ll
#['Moi', 15.5, 2021, 'Moi', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']

for i in ll:
    if isinstance(i, int):
        print(f"Yes '{i}' is an int")
    elif isinstance(i, str):
        if i in ['world', 'Luis']:
            print(f"String : '{i}' => OK")
    else:
        print(f"Neither str or int : '{i}'")

#Neither str or int : '15.5'
#Yes '2021' is an int
#Yes '3' is an int
#Yes '7' is an int
#String : 'Luis' => OK


# >>> for i in ll:
# ...     if isinstance(i, int):
# ...         print(f"Yes '{i}' is an int")
# ...     elif isinstance(i, str):
# ...             print(f"String : '{i}' => OK")
# ...     else:
# ...         print(f"Neither str or int : '{i}'")
# ...
# String : 'Moi' => OK
# Neither str or int : '15.5'
# Yes '2021' is an int
# String : 'Moi' => OK
# String : 'z' => OK
# String : 'o' => OK
# String : 'e' => OK
# Yes '3' is an int
# Yes '7' is an int
# String : 'Luis' => OK
# String : 'RR' => OK


## Looping with comprehension list
# =================================

ll
#['Moi', 15.5, 2021, 'Moi', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']
res = []
for i in ll:
    if not isinstance(i, str):
        res.append((i, i-1))
print(res)
#[(2021, 2020), (3, 2), (7, 6)]

res =[]
for i in ll:
    if isinstance(i, str):
        res.append(i.upper())
print(res)
#['MOI', 'MOI', 'Z', 'O', 'E', 'LUIS', 'RR']


# Each one can be done in one line

ll
# [22, 3.14, 'world', 'z', 'o', 'e', 3, 7, 'Luis', 'RR']
res = []
[(i, i-1) for i in ll if not isinstance(i, str)]
#[(15.5, 14.5), (2021, 2020), (3, 2), (7, 6)]
[i.upper() for i in ll if isinstance(i, str)]
#['MOI', 'MOI', 'Z', 'O', 'E', 'LUIS', 'RR']

# Exercice barely understood
# ---------------------------


# Dictionares comprehension
# ==========================

dd = {'a': 42, 'pi': 3.14, 'nom': 'Luis', 'age': 33}
{k:v+1 for k, v in dd.items() if k not in ['nom']}
#{'a': 43, 'pi': 4.140000000000001, 'age': 34}
{k:int(v+10) for k, v in dd.items() if k not in ['nom']}
#{'a': 52, 'pi': 13, 'age': 43}

## Creating dict more in comprehensions' way

{k: f'file_{k}' for k in range(1, 4)}
#{1: 'file_1', 2: 'file_2', 3: 'file_3'}

## Creating dic from list of tuples and keywords

lt = [('a', 42), ('pi', 3.14)]
dict(lt, nom='Luis', age=33)
#{'a': 42, 'pi': 3.14, 'nom': 'Luis', 'age': 33}

# Very simple, in fact, tuples are like items


## Creating a dict from a dict and keywords
d
#{'age': 33, 'name': 'Luis'}
dd = d.copy()
dd
#{'age': 33, 'name': 'Luis'}
dd.update(name='Paul', age=42, town='Paris')
dd
#{'age': 42, 'name': 'Paul', 'town': 'Paris'}
lt
#[('a', 42), ('pi', 3.14)]
dict(lt, **dd)
#{'a': 42, 'pi': 3.14, 'age': 42, 'name': 'Paul', 'town': 'Paris'}

# So, the thing is to create a copy of a preexistant dict, improve it
# (using .update) then add the keywords (the list of tuples) with dict(lt, **dd)

print()
print()

class Pet(object):
        "Our first Class"
        # The constructor is the method named __init__
        def __init__(self, name, eat='eggs'):
            "This method is constructor"
            self.name = name
            self.eat = eat
        # show_msg is a class variable
        show_msg = "I am {}, a Pet, I eat {}"
        # A class method is just a fonction with the self as first variable
        def show(self, msg=None):
            "This is a method of the Pet Class"
            if not msg:
                print(Pet.show_msg.format(self.name, self.eat))
            else:
                print(msg.format(self.name, self.eat))

pets = [Pet(name) for name in ['Bob', 'Mick', 'Yan', 'Tony']]
pp = pets[0]

for pet in pets:
    pet.show()
    if len(pet.name) > 3:
        pet.eat = 'bacon'
        pet.show('Hey, now {} eats {}')
    else:
        print('  my name is short')



class Vehicules():
    def __init__(self, marque='Renault'):
        "This method is constructor"
        print("  Je suis le constructeur ha ha je m'appel __init__")
        self.marque = marque

class Voitures(Vehicules):
    def __init__(self, marque='Renault', roues=4):
        Vehicules.__init__(self)
        self.roues = roues

class Camions(Voitures):
    def __init__(self, marque='Renault'):
        Voitures.__init__(self)
        self.remorque = True
        self.charges_utile_en_tonnes = 10

cc = Camions()



