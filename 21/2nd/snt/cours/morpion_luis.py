plt_full = [(0, 0, "x"), (0, 1, "x"), (0, 2, "o"),
            (1, 0, "o"), (1, 1, "o"), (1, 2, "x"),
            (2, 0, "x"), (2, 1, "o"), (2, 2, "x")]

# no winner not yet full
plt2 = [(0, 0, "x"), (0, 1, "x"), (0, 2, "o"),
        (1, 0, "o"), (1, 1, "o"), (1, 2, "x"),
        (2, 0, "x"), (2, 1, "o"), (2, 2, "")]

# x win, line 0
plt3 = [(0, 0, "x"), (0, 1, "x"), (0, 2, "x"),
        (1, 0, "o"), (1, 1, "o"), (1, 2, "x"),
        (2, 0, "o"), (2, 1, "o"), (2, 2, "")]

# o win, diagonale
plt4 = [(0, 0, "o"), (0, 1, "o"), (0, 2, "x"),
        (1, 0, "o"), (1, 1, "x"), (1, 2, "x"),
        (2, 0, "x"), (2, 1, "o"), (2, 2, "")]


# o win, column 1
plt5 = [(0, 0, "x"), (0, 1, "o"), (0, 2, "x"),
        (1, 0, "o"), (1, 1, "o"), (1, 2, "x"),
        (2, 0, "o"), (2, 1, "o"), (2, 2, "")]


# x win, line 1
plt6 = [(0, 0, "o"), (0, 1, "x"), (0, 2, "x"),
        (1, 0, "x"), (1, 1, "x"), (1, 2, "x"),
        (2, 0, "o"), (2, 1, "o"), (2, 2, "")]

import ipdb

def check(plt):
    out = [p[-1] for p in plt]
    if all(out):
        print("Plateau is full, nobody won")
        return
    for piece in ('x', 'o'):
        # ipdb.set_trace()
        # breakpoint()
        for line in range(0, 1, 2):
            print(piece, line, [piece for x, y, piece in plt if x == line], [f"{piece}", f"{piece}", f"{piece}"])
            if [piece for x, y, piece in plt if x == line] == [f"{piece}", f"{piece}", f"{piece}"]:
                print(f"{piece} won, line {line}")
                return
        for column in range(0, 1, 2):
            if [piece for x, y, piece in plt if y == column] == [f"{piece}", f"{piece}", f"{piece}"]:
                print(f"{piece} won, column {column}")
                return
        if [piece for x, y, piece in plt if x == y] == [f"{piece}", f"{piece}", f"{piece}"]:
            print(f"{piece} won, anti-diagonale")
            return
        elif [piece for x, y, piece in plt if x+y == 2] == [f"{piece}", f"{piece}", f"{piece}"]:
            print(f"{piece} won, diagonale")
            return
        else:
            print("Next player")
            return

def better_check(plt):
    out = [p[-1] for p in plt]
    if all(out):
        print("Plateau is full, nobody won")
        return

    lines = []

    for idx, droite in enumerate(range(3)):
        lines.append((f'l{idx}', [pce for x, y, pce in plt if x == droite]))
        lines.append((f'c{idx}', [pce for x, y, pce in plt if y == droite]))
    lines.append(('D0', [pce for x, y, pce in plt if x == y]))
    lines.append(('D1', [pce for x, y, pce in plt if x+y == 2]))
    lines.sort()

    show(plt)

    for name, trait in lines:
        print(name, trait)
        if trait == ['x', 'x', 'x']:
            print(f'x won with: {name}')
            return
        if trait == ['c', 'c', 'c']:
            print(f'c won with: {name}')
            return
    print('nobody won yet next player')
    return


def show(plt):
    for idx, line in enumerate(plt):
        new_line = idx % 3 == 2 and '\n' or ' '
        print(line[-1],  end=new_line)
    print()


better_check(plt2)

# # no winner not yet full "Next player" expected
# check(plt2)
# # x win, line 0
# check(plt3)
# # o win, diagonale
# check(plt4)
# # o win, column 1
# check(plt5)
# # x win, line 1
# # check(plt6)
## ===>  /!\ PLT 5 AND 6 DOESNT WORK /!\  <===
