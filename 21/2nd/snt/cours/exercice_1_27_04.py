l = dir(__builtins__)
print(l)

# exercice, write items as 3 tuples
# tip, builtin enumerate

print(enumerate.__doc__)
ll = [len(s) for s in l]
ll.sort()
max_size = ll[-1]
for idx, i in enumerate(l):
    print(f"builtin : {i:{max_size}} rang : {idx}")

# builtin : sum                       rang : 146
# builtin : super                     rang : 147
# builtin : tuple                     rang : 148
# builtin : PendingDeprecationWarning rang : 43
# builtin : type                      rang : 149
# builtin : vars                      rang : 150
# builtin : zip                       rang : 151

In [52]: while lll:
    ...:     for i in range(3):
    ...:         if lll:
    ...:             print(lll.pop(), end=' ')
    ...:     print()

zip vars type
tuple super sum
str staticmethod sorted
slice setattr set
round reversed repr
range property print
pow ord


