Entr_py_functions

=================================
## FUNCTIONS, METHODS AND CLASSES
=================================

def my_plus(x, y=2, *optional_list, **optional_dict):
...         "my_plus illustrates a python function"
...         result = []
...         # y has the default value: 2
...         result.append('{} plus {} is {}'.format(x, y, x + y))
#okay, there i understand
            if optional_list:
...             msg = 'list: len: {} items: {}'
...             result.append(msg.format(len(optional_list), optional_list))
#didnt understood, why there is not f-string ?
            if optional_dict:
...             msg = ('dict: len: {len(optional_dict)};  '
...                    '  keys: {k};  '
...                    '  values: {v}')
...             k = optional_dict.keys()
...             v = optional_dict.values()
...             result.append(msg)
...         for line in result:
...             print(line)
#okay, there i understood


===================================
## Classes, constructors, instances
===================================
# The constructor is the method named __init__
