from random import randint

ll = [randint(0, 9) for i in range(12)]
ll
# [1, 1, 7, 1, 2, 4, 4, 7, 6, 0, 9, 5]

def qs(our_list):
    ll = our_list.copy()
    # print('==> New list:', ll)
    if not ll:
        return []
    else:
        elem = ll.pop()
        smaller = [i for i in ll if i < elem]
        bigger = [i for i in ll if i >= elem]
        print(f'  elem: {elem} smaller: {smaller} bigger: {bigger}')
        return qs(smaller) + [elem] + qs(bigger)

print(f'list to sort: {ll}')
qs(ll)
# [0, 1, 1, 1, 2, 4, 4, 5, 6, 7, 7, 9]

def qs2(our_list):
    ll = our_list.copy()
    if len(ll) == 1:
        return ll
    print('==> New list:', ll)
    elem = ll.pop()
    smaller = [i for i in ll if i < elem]
    bigger = [i for i in ll if i >= elem]
    print(f'  elem: {elem} smaller: {smaller} bigger: {bigger}')
    left = qs2(smaller) if smaller else []
    right = qs2(bigger) if bigger else []
    return left + [elem] + right

