## Entr_py_3

s, l, d = "Bonjour", ["Moi", 15.5, 2021, "Moi"], {"age": 15, "nom": "Moi"}
s
#'Bonjour'

## String API

print("s.capitalize() ==> OK")
s = 'bonjour'
s.capitalize()
#'Bonjour'

print("s.casefold() ==> OK")
s = 'Bonjour'
s.casefold()
#'bonjour'

print("s.center() ==> OK")
s = 'Bonjour'
s.center(50)
#'                     Bonjour                      '

print("s.count() ==> KO")

print("s.encode() ==> KO")

print("s.endswith() ==> KO")

print("s.expandtabs() ==> KO")

print("s.find() ==> KO")

print("s.format() ==> KO")

print("s.format_map() ==> KO")

print("s.index() ==> OK")
s
#'Bonjour'
s.index("j")
#3
s.index("r")
#6
s.index("o")
#1

print("s.isalnum() ==> OK")
s
#'Bonjour'
s.isalnum()
#True
s = "3.45"
s.isalnum()
#False
s = "567"
s.isalnum()
#True
s = "5Bonjour6"
s.isalnum()
#True

print("s.isalpha() ==> OK")
s = "Bonjour"
s.isalpha()
#True
s = "567"
s = "3.45"
s.isalpha()
#False
s = "5Bonjour6"
s.isalpha()
#False

print("s.isascii() ==> OK")
s = "Bonjour"
s.isascii()
#True
s = "567"
s.isascii()
#True
s = "3.45"
s.isascii()
#True
s = "5Bonjour6"
s.isascii()
#True

print("s.isdecimal() ==> OK")
s = "5Bonjour6"
s.isdecimal()
#False
s = "3.45"
s.isdecimal()
#False
s = "0.003"
s.isdecimal()
#False
s = "0.001"
s.isdecimal()
#False
s = "10"
s.isdecimal()
#True
s = "100"
s.isdecimal()
#True

print("s.isdigit() ==> OK")
s = "Bonjour"
s.isdigit()
#False
s = "3.14"
s.isdigit()
#False
s = "3"
s.isdigit()
#True
s = "356"
s.isdigit()
#True

print("s.isidentifier() ==> OK")
s = "Bonjour"
s.isidentifier()
#True
s = "Bonjour4"
s.isidentifier()
#True
s = "2"
s.isidentifier()
#False
s = "2.23"
s.isidentifier()
#False

print("s.islower() ==> OK")
s = "Bonjour"
s.islower()
#False
s = "bonjour"
s.islower()
#True

print("s.isnumeric() ==> OK")
s = "345"
s.isnumeric()
#True
s = "3.45"
s.isnumeric()
#False
s = "3bonjour"
s.isnumeric()
#False
s = "bonjour"
s.isnumeric()
#False

print("s.isprintable) ==> KO")

print("s.isspace() ==> OK")
s = "bonjour"
s.isspace()
#False
s = ""
s.isspace()
#False
s = " "
s.isspace()
#True

print("s.istitle() ==> OK")
s = "bonjour"
s.istitle()
#False
s = "Bonjour"
s.istitle()
#True
s = "BONJOUR"
s.istitle()
#False

print("s.isupper() ==> OK")
s = "BONJOUR"
s.isupper()
#True
s = "Bonjour"
s.isupper()
#False
s = "bonjour"
s.isupper()
#False

print("s.join() ==> OK")
s = "bonjour"
s.join("ab")
#'abonjourb'

print("s.ljust() ==> OK")
s = "bonjour"
s.ljust(30," ")
#'Bonjour                       '

print("s.lower() ==> OK")
s = "BONJOUR"
s.lower()
#'bonjour'

print("s.lstrip() ==> KO")

print("s.maketrans() ==> KO")

print("s.partition() ==> OK")
s
#'bonjour'
s.partition("j")
#('bon', 'j', 'our')

print("s.replace() ==> KO")

print("s.rfind() ==> OK")
s
#'bonjour'
s.rfind("n")
#2
s.rfind("r")
#6

print("s.rindex() ==> KO")

print("s.rjust() ==> OK")
s
#'bonjour'
s.rjust(30, " ")
#'                       bonjour'

print("s.rpartition() ==> OK")
s
#'bonjour'
s.rpartition("n")
#('bo', 'n', 'jour')

print("s.rsplit() ==> OK")
s
#'bonjour'
s.rsplit("n")
#['bo', 'jour']

print("s.rstrip() ==> KO")

print("s.split() ==> OK")
s
#'bonjour'
s.split("n")
#['bo', 'jour']

print("s.splitlines() ==> KO")

print("s.startswith() ==> KO")

print("s.strip() ==> KO")

print("s.swapcase() ==> OK")
s = "Bonjour"
s.swapcase()
#'bONJOUR'

print("s.title() ==> OK")
s = "Bonjour les amis"
s.title()
#'Bonjour Les Amis'

print("s.translate() ==> KO") #need make trans

print("s.upper() ==> OK")
s
#'Bonjour les amis'
s.upper()
#'BONJOUR LES AMIS'

print("s.zfill() ==> OK")
s
#'Bonjour les amis'
s.zfill(30)
#'00000000000000Bonjour les amis'

