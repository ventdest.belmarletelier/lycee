#Entraînement Python 1 12/04/21

## Strings, lists and dictionnaries

s, l, d = "Bonjour", ["Moi", 15.5, 2021], {"age": 15, "nom": "Moi"}

s, l, d
# ('Bonjour', ['Moi', 15.5, 2021], {'age': 15, 'nom': 'Moi'})

## Tuples

t = ("Salut", 14.9, 2020)

t
#('Salut', 14.9, 2020)

## f-strings


## APIs

# dict API 

print("d.clear() ==> OK")

d
#{'age': 15, 'nom': 'Moi'}
d.clear()
d
#{}

print("d.copy() ==> KO")

print("d.fromkeys() ==> KO")

print("d.get() ==> OK")

d
#{'age': 15, 'nom': 'Moi'}
d.get(1)
d.get("maison")
d.get("age")
#15

print("d.items() ==> OK")

d
#{'age': 15, 'nom': 'Moi'}
d.items()
#dict_items([('age', 15), ('nom', 'Moi')])

print("d.keys() ==> OK")

d
#{'age': 15, 'nom': 'Moi'}
d.keys()
#dict_keys(['age', 'nom'])

print("d.pop() ==> OK")

d
#{'age': 15, 'nom': 'Moi'}
d.pop('nom')
#'Moi'
d
#{'age': 15

print("d.popitem() ==> OK")

d
#{'age': 15, 'nom': 'Moi'}
d.popitem()
#('nom', 'Moi')
d
#{'age': 15}
d.popitem()
#('age', 15)
d
#{}

print("d.setdefault() ==> OK")

d
#{'age': 15, 'nom': 'Moi'}
d.setdefault("truc")
d
#{'age': 15, 'nom': 'Moi', 'truc': None}
d.setdefault('age')
#15
d
#{'age': 15, 'nom': 'Moi', 'truc': None}

print("d.update() ==> KO")

print("d.values() ==> OK")

d
#{'age': 15, 'nom': 'Moi'}
d.values()
#dict_values([15, 'Moi'])

