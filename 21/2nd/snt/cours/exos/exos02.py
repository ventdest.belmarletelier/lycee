exo2 = """
La variable « text » contient la phrase suivante : "On avait sûrement calomnié
Joseph K., car, sans avoir rien fait de mal, il fut arrêté un matin."

Votre programme devra:

    compter le nombre de caractère de la phrase
    compter le nombre de voyelles
    compter le nombre de consonnes
    compter le nombre de ponctuation
    compter le nombre de chiffres
    compter le nombre d’espaces
"""

print(',' in 'car, sans')
