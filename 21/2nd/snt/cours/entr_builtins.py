## Entr_py_4


print(dir(__builtins__))
#['ArithmeticError', 'AssertionError', 'AttributeError', 'BaseException', 'BlockingIOError', 'BrokenPipeError', 'BufferError', 'BytesWarning', 'ChildProcessError', 'ConnectionAbortedError', 'ConnectionError', 'ConnectionRefusedError', 'ConnectionResetError', 'DeprecationWarning', 'EOFError', 'Ellipsis', 'EnvironmentError', 'Exception', 'False', 'FileExistsError', 'FileNotFoundError', 'FloatingPointError', 'FutureWarning', 'GeneratorExit', 'IOError', 'ImportError', 'ImportWarning', 'IndentationError', 'IndexError', 'InterruptedError', 'IsADirectoryError', 'KeyError', 'KeyboardInterrupt', 'LookupError', 'MemoryError', 'ModuleNotFoundError', 'NameError', 'None', 'NotADirectoryError', 'NotImplemented', 'NotImplementedError', 'OSError', 'OverflowError', 'PendingDeprecationWarning', 'PermissionError', 'ProcessLookupError', 'RecursionError', 'ReferenceError', 'ResourceWarning', 'RuntimeError', 'RuntimeWarning', 'StopAsyncIteration', 'StopIteration', 'SyntaxError', 'SyntaxWarning', 'SystemError', 'SystemExit', 'TabError', 'TimeoutError', 'True', 'TypeError', 'UnboundLocalError', 'UnicodeDecodeError', 'UnicodeEncodeError', 'UnicodeError', 'UnicodeTranslateError', 'UnicodeWarning', 'UserWarning', 'ValueError', 'Warning', 'ZeroDivisionError', '__IPYTHON__', '__build_class__', '__debug__', '__doc__', '__import__', '__loader__', '__name__', '__package__', '__spec__', 'abs', 'all', 'any', 'ascii', 'bin', 'bool', 'breakpoint', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'compile', 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'display', 'divmod', 'enumerate', 'eval', 'exec', 'filter', 'float', 'format', 'frozenset', 'get_ipython', 'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int', 'isinstance', 'issubclass', 'iter', 'len', 'license', 'list', 'locals', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print', 'property', 'range', 'repr', 'reversed', 'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum', 'super', 'tuple', 'type', 'vars', 'zip']

builtins = [x for x in (dir(__builtins__)) if not x[0].isupper() if not x.startswith("__")]

# Study the builtins

print(builtins)
#['abs', 'all', 'any', 'ascii', 'bin', 'bool', 'breakpoint', 'bytearray', 'bytes', 'callable', 'chr', 'classmethod', 'compile', 'complex', 'copyright', 'credits', 'delattr', 'dict', 'dir', 'display', 'divmod', 'enumerate', 'eval', 'exec', 'filter', 'float', 'format', 'frozenset', 'get_ipython', 'getattr', 'globals', 'hasattr', 'hash', 'help', 'hex', 'id', 'input', 'int', 'isinstance', 'issubclass', 'iter', 'len', 'license', 'list', 'locals', 'map', 'max', 'memoryview', 'min', 'next', 'object', 'oct', 'open', 'ord', 'pow', 'print', 'property', 'range', 'repr', 'reversed', 'round', 'set', 'setattr', 'slice', 'sorted', 'staticmethod', 'str', 'sum', 'super', 'tuple', 'type', 'vars', 'zip']


## ABS

#Return the absolute value of the argument.
l = [2, -54, 77.7]
abs(l[0])
#2
abs(l[2])
#77.7
abs(l[1])
#54
# ==> can be used on lists like this

# Errors
abs([2, -54, 77.7])
#TypeError: bad operand type for abs(): 'list'
# ==> abs can't be used on lists or tuples
abs(2, -54, 77.7)
#TypeError: abs() takes exactly one argument (3 given)
# ==> abs takes one argument


## ALL

#Return True if bool(x) is True for all values x in the iterable.
#If the iterable is empty, return True.
all([2, -54, 77.7])
#True
all([2, -54, 77.7, 0])
#False
d.items()
#dict_items([('age', 34), ('nom', 123)])
all(d.items())
#True

# Errors
all(3)
#TypeError: 'int' object is not iterable
all("3")
#True
all("3", 3, "", "A")
#TypeError: all() takes exactly one argument (4 given)


## ANY

#Return True if bool(x) is True for any x in the iterable.
#If the iterable is empty, return False.
# ==> contrary of ALL

any([2, -54, 77.7, 0])
#True


## ASCII

#Return an ASCII-only representation of an object.
#As repr(), return a string containing a printable representation of an
#object, but escape the non-ASCII characters in the string returned by
#repr() using \\x, \\u or \\U escapes.

repr(3.132)
#'3.132'
ascii(3.132)
#'3.132'
ascii(l)
#'[2, -54, 77.7]'


## BIN

# return binary repr

## BOOL

#Returns True when the argument x is true, False otherwise.
#The builtins True and False are the only two instances of the class bool.
#The class bool is a subclass of the class int, and cannot be subclassed.

## BREAKPOINT

#breakpoint(*args, **kws)
#Call sys.breakpointhook(*args, **kws).  sys.breakpointhook() must accept
#whatever arguments are passed.
#By default, this drops you into the pdb debugger.
# ==> debugger, dans une fonction, avant le truc a debugger

## BYTEARRAY
#créer un byte
#(array = tableau)

## BYTES
#donne une repr de str telle qu'elle est vue en bytes

## CALLABLE

#Return whether the object is callable (i.e., some kind of function).
#Note that classes are callable, as are instances of classes with a
#__call__() method.
# callable(x)

## CHR ord
#chr ==> byte => resultat of the byte
#ord ==> resultat of the byte => byte

## classmethod
#programmation orientée objet

## compile et eval
#Compile source into a code object that can be executed by exec() or eval().
# compil => voir eval
# interpret str comme si ct une commande

## complex
# crée un nombre complexe
complex(3, 5)
#(3+5j)

## delattr getattr hasattr
# hasattr, return true/flase if 2dn arg is attribute of the first
hasattr(__builtins__, 'dir')
#True

class Vache():
    def normande(self):
        print("miaou la vache")
vache22 = Vache()
vache22.normande()
# miaou la vache

getattr(vache22, "normande")
#<bound method Vache.normande of <__main__.Vache object at 0x7f86fc296460>>
getattr(vache22, "normande")()
#miaou la vache

 setattr(vache22, "robe", "noire")
 vache22.robe
 'noire'

