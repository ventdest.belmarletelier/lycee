# I Intro python
""" 1 Arithmétique, variables, instructions
1.1 Mode interactif
1.2 Mode programme
1.3 Bibliothèque Turtle
Exercices

#1.3 Turtle
"""
import random as r
from turtle import*
def carre(d = 42):
    """nous allons dessiner un carré de côté 42 avec turtle"""
    a = 90
    for i in range(4):
        forward(d)
        right(a)
    goto(0, 0)
    return

#carre()


def cercle(d = 42):
    """ nous allons dessiner un cercle de rayon 42 avec turtle"""
    circle(d, 360)
    goto(0,0)
    return

#cercle()
"""
#dot(r) sert à dessiner un point (un disque) de rayon r.


up() => relever le crayon (et donc interrompre le dessin)
down() => redescendre le crayon (et donc reprendre le dessin)
width(e) => fixer à e l'épaisseur du trait
color("c") => selectionner la couleur c pour les traits
begin_fill() => activer le mode remplissage
end_fill() => désactiver le mode remplissage
fillcolor("c") => selectionner la couleur c pour le remplissage
"""

def rectangle_epais():
    width(5)
    color(1, 0, 0.3)
    left(90)
    for i in range(2):
        forward(150)
        right(90)
        forward(90)
        right(90)
    return

#rectangle_epais()

def sablier_bleu():
    rectangle_epais()
    up()
    goto(0,0)
    begin_fill()
    fillcolor("blue")
    width(2)
    goto(9, 15)
    down()
    goto(81, 135)
    goto(9, 135)
    goto(81, 15)
    goto(9, 15)
    up()
    end_fill()
    goto(0,0)
    return


#sablier_bleu()

"""
reset() => tt effacer, recommence à 0
speed(s) => vitesse de déplacement du curseur avec s entre 0 et 10
title(t) => titre t à la fenêtre de dessin
ht() => hide turtle
"""

#Exercices
"""
Exercice 1:
-----------
In [1]: 5 - 3 - 2
Out[1]: 0

In [2]: 1 / 2 / 2
Out[2]: 0.25

On voit que, sans parenthèses, Python interprètes les calculs dans l'ordre dans
lequels ils sont donnés. Il met des parenthèses automatiquement 'autour' des
div. et mult.

In [5]: (1 + 1/2) * 3
Out[5]: 4.5

In [7]: 1 + 1/2 * 3
Out[7]: 2.5

==================

Exercice 2:
-----------

1.
In [9]: 1 + 2 * 3 - 4
Out[9]: 3
In [10]: 1 + (2 * 3) - 4
Out[10]: 3
2.
In [11]: 1+2 / 4*3
Out[11]: 2.5
In [12]: 1 + (2/4) * 3
Out[12]: 2.5
3.
In [14]: a = 2
In [15]: 1 - a+a*a/2-a*a*a/6+a*a*a*a/24
Out[15]: 0.33333333333333337
In [16]: 1 - a + (a*a)/2 - (a*a) * (a/6) + (a*a)*(a*a) / 24
Out[16]: 0.33333333333333337

Exercice 3:
-----------
Boring

Exercice 4:
-----------

a = 6
In [3]: a = 2
In [4]: a = 4
In [5]: a = a+2
In [6]: a
Out[6]: 6

Exercice 5:
-----------

b = 64
In [7]: a = 2
In [8]: b = a*a
In [9]: b = a*b
In [10]: b = b*b
In [11]: b
Out[11]: 64

Exercice 6:
-----------
Boring

Exercice 7:
-----------

1. print("1+") affiche 1+
2. print(1+) affiche une erreur "invalid syntax"

Exercice 8:
-----------

Python affiche : TypeError: can only concatenate str (not "int") to str
Car il ne peut pas ajouter 1 à la chaîne de caractères "5"
Il faut écricre :
a = int(input("saisir un nombre : "))
print("le nombre suivant est ", a+1)

In [18]: a = int(input("saisir un nombre : "))
saisir un nombre : 5
In [19]: print("le nombre suivant est ", a+1)
    ...:
le nombre suivant est  6

Exercice 9:
-----------

Cette séquence d'instruction inverse les valeurs de a et b.
In [25]: a = 2
In [26]: b = 3
In [27]: tmp = a
In [28]: tmp
Out[28]: 2
In [29]: a = b
In [30]: a
Out[30]: 3
In [31]: tmp
Out[31]: 2
In [32]: b
Out[32]: 3
In [33]: b = tmp
In [34]: b
Out[34]: 2
In [35]: a
Out[35]: 3
In [36]: b
Out[36]: 2
In [37]: tmp
Out[37]: 2

Exercice 10:
------------

a et b son aussi inversés, mais cette fois sans utiliser de variable
intermédiaire.
In [46]: a = 2
In [47]: b = 3
In [48]: a = a + b
In [49]: a
Out[49]: 5
In [50]: b = a - b
In [51]: b
Out[51]: 2
In [52]: a = a - b
In [53]: a
Out[53]: 3
In [54]: b
Out[54]: 2

Exercice 11:
------------
"""
def aire_rect():
    l = int(input( "entrez la largueur d'un rectangle : "))
    L = int(input( "entrez un longueur d'un rectangle : "))
    print(f"voici l'aire de votre rectangle {l*L}")
    return

#aire_rect()

"""
Exercice 12:
------------
"""
def conversion_bases():
    base = int(input("Entrez une base entre 2 et 36: "))
    nb = input("Entrez un nombre dans cette même base: ")
    nb_10 = int(nb, base)
    print("voici votre nombre en base 10 ",nb_10)
    return

#conversion_bases()

"""
Exercice 13:
------------
"""
def heure():
    s = int(input("Entrez un nombre de secondes : "))
    m = s//60
    s = s%60
    h = m//60
    m = m%60
    print(f"voici l'heure en h/m/s : {h}/{m}/{s}")
    return

#heure()
"""
Exercice 14:
------------

n = int(input("combien d'œufs : "))
print(n//6)

1. ce programme ne fonctionne qu'avec les multiples de 6.
2. car ce n'est pas toujours le cas (que il faut arrondir à la boite d'œufs au
dessus)
3.... je vois pas vraiment, il faudrait utiliser "if" mais on est pas censé le
faire..
"""
def boites():
    n = int(input("combien d'œufs : "))
    if 0 < n%6 < 6:
        print(n//6 + 1)
    else:
        print(n//6)
    return

"""
Exercice 15:
------------
"""
def intersection_droites():
    a = int(input("Entrez un nombre, ce sera la pente de la première droite: "))
    b = int(input("Entrez un nombre, ce sera la l'ordonnée à l'origine de la première droite: "))
    c = int(input("Entrez un nombre, ce sera la pente de la deuxième droite: "))
    d = int(input("Entrez un nombre, ce sera la l'ordonnée à l'origine de la première droite: "))
    if a != c:
        x = (d-b)/(a-c)
        y = (a*(d-b)/(a-c))+b
        i = (x, y)
        print(f"voici l'intersection de ces deux droites : {i}")
    else:
        print(f"ces deux droites n'ont pas d'intersection, elles sont parallèles.")
    return

#intersection_droites()

"""
Exercice 16:
------------
"""

def rectangle():
    width(5)
    color(1, 0, 0.3)
    left(90)
    for i in range(2):
        forward(90)
        right(90)
        forward(150)
        right(90)
    return

# rectangle()

def hexagone():
    width(5)
    color("red")
    right(90)
    for i in range(6):
        forward(50)
        left(60)
    left(90)
    return

# hexagone()

def etoile():
    width(5)
    color("red")
    up()
    goto(0, 0)
    down()
    goto(50, 100)
    up()
    goto(0, 100)
    down()
    goto(50, 0)
    up()
    goto(100, 50)
    down()
    goto(-50, 50)
    up()
    goto(0, 0)
    down()
    return

# etoile()

"""
Exercice 17:
------------
cela fait une sorte de maisonnette
"""
def ex_17():
    speed(1)
    goto(20, 0)
    goto(0, 20)
    goto(20, 20)
    goto(0, 0)
    goto(0, 20)
    goto(10, 30)
    goto(20, 20)
    goto(20, 0)
    return

# ex_17()

"""
Exercice 18:
------------
Flemme...


# CHAPITRE 2
# Boucle for

# 2.1 Dessiner une spirale
"""
def spirale():
    r = 5
    g = 0.1
    w = 5
    width(w)
    down()
    for i in range(9):
        circle(r, 180)
        r += 10
        g += 0.1
        color(0, g, 0.5)
        w += 2
        width(w)
    return

# spirale()
"""
2.2 Boucles bornées simples
"""
def boucles_boucles_bornees_simples():
    for _ in range(3): print("A")
    n = int(input())
    for _ in range(2*n) : print("a")
    return

# boucles_bornees_simples()

def boucles_bs_bloc():
    n = int(input("Combien de segments? : "))
    for _ in range(n -1):
        x = r.randint(-100, 100)
        y = r.randint(-100, 100)
        goto(x, y)
    goto(0, 0)
    return

# boucles_bs_bloc()

"""
2.3 Utilisation de l'indice de boucle
"""

def indice_b():
    for i in range(10):
        print(i)
    return

# indice_b()

"""
Meilleure spirale
"""
def spirale_op():
    n = int(input("Combien de tours de spirale ? : "))
    for i in range(2*n):
        circle(i*i, 180)
        width(i)
    return

# spirale_op()

"""
2.4 Utilisation d'un accumulateur

Un accumulateur est une variable dont la valeur évolue à chaque tour de boucle
et qui change l'etat du programme.
"""

def ex_acc():
    a = 1
    for i in range(4):
        a = a + 2
        print(a)
    return

# ex_acc()

"""
Calcul de moyenne
"""

def moyenne_classe():
    n = int(input("Nombre d'eleves ? : "))
    somme = 0
    for i in range(n):
        e = float(input(f"Note eleve {i+1} (avec decimales) : "))
        somme += e
    moyenne = somme / n
    print(f"La moyenne de la classe est {moyenne}")
    return

# moyenne_classe()

"""
Exercices

Exercice 22:
"""
def puiss_de_2():
    n = int(input("Entrez un nombre quelquonque : "))
    r = 1
    for i in range(n):
        r = 2*r
    print(r)
    return

# puiss_de_2()

"""
Exercice 23:
"""
def ex_23():
    p = 1
    for i in range(1, 100, 1):
        p = p*i
    print(p)
    return

# ex_23()

"""
Exercice 24:
"""
def somme_suite_ar():
    n = int(input("Entrez un nombre n : "))
    somme = 0
    for i in range(1, n+1, 1):
        somme += i
        print(somme)
    print(f"somme = {n*(n+1)//2}")
    return

# somme_suite_ar()

"""
Exercice 25:
"""
def banque_1():
    s = int(input("Somme initiale déposée sur le livret (en euros) : "))
    t = float(input("Taux d'interêts annuel (en pourcents) : "))
    n = int(input("Sur combien d'années ? : "))
    for i in range(n):
        print(f"Interêts perçus cette année: {s*t/100}")
        s += s*t/100
        print(f"Montant présent à la {i+1}ème année : {s}")
    print(f"Montant total présent sur le livret : {s}")
    return

# banque_1()

"""
Exercice 26:
"""
def moy_pond():
    n = int(input("Combien de matières ? : "))
    for i in range(n):
        note = float(input("Entrez une note : "))
        c = float(input("Entrez son coeficient : "))
    print(f"Voici la moyenne générale pondérée : {(note*c)/n}")
    return

# moy_pond()

"""
Exercice 27:

def ex_27a():
    n = int(input("Combien de nombres ? : "))
    for i in range(n):
        c = int(input("nombre (à un chiffre) : "))
        print(c, end="")
    return

ex_27a()

def ex_27b():
    n = int(input("Combien de nombres ? : "))
    for i in range(n):
        c = int(input(f"{i+1}° nombre (à un chiffre) : "))

Ex 27 doesnt works

Exercice 28:
"""
def ex_28():
    n = int(input("Entrez un nombre entier : "))
    k = int(input("Entrez un nombre de chiffres : "))
    for i in range(k):
        print(f"{n%10}")
        n = n // 10
    return

# ex_28()

"""
Exercice 29:
Déjà fait

Exercice 30:
"""

def hexagone2():
    width(5)
    color("red")
    right(90)
    c = int(input("Nombre de côtés du polygone : "))
    for i in range(c):
        forward(50)
        left(360/c)
    return

# hexagone2()


def etoile2():
    b = int(input("Nombre de branches : "))
    for i in range(b):
        up()
        goto(0,0)
        down()
        forward(50)
        up()
        goto(0,0)
        down()
        backward(50)
        left(360/b)
    return

# etoile2()

"""
Exercice 31:
"""
def escalier():
    n = int(input("Nombre de marches : "))
    up()
    goto(0,0)
    for i in range(n):
        down()
        forward(50)
        right(90)
        forward(50)
        left(90)
    return

# escalier()

"""
Exercice 32
"""

def grille():
    n = int(input("Combien de lignes : "))
    m = int(input("Combien de colonnes : "))
    c = 20
    up()
    goto(0,0)
    down()
    for i in range(0, n+1):
        up()
        goto(0, c*i)
        down()
        goto(c*m+1, c*i)
    for i in range(0, m+1):
        up()
        goto(c*i, 0)
        down()
        goto(c*i, c*n+1)
    return

# grille()

"""
Exercice 35
Pris la correction, mais j'ai pas compris :/
"""
def fib():
    n = int(input("Entrez un entier n >= 1 : "))
    fib_na = 0 # valeur en Fn-1
    fib_nb = 1 # valeur en Fn
    for i in range(2, n+1):
        fib_na = fib_nb
        fib_nb = fib_na + fib_nb
    print(fib_na)
    return

# fib()

"""

Chapitre 3
Comparaisons, booléens, tests

3.2 Conditions et Branchements

if, condition ("si"), l'action suivante (dans le bloc) est effectuée que si la
condition est vraie.

-------------

notations
>
<
>=
<=
==
!=

-------------

else, tout ce qui n'est pas dans la condition if, ("sinon")

elif, si if est faux, faire ça, else if ("sinon si")

exemple
"""
def cond_branch():
    n = int(input("Entrez un nombre : "))
    if n < 0:
        print("négatif")
    elif n == 0:
        print("nul")
    elif n >= 2:
        print("grand")
    else:
        print("petit")
    return

# cond_branch()

"""
3.4 Expressions booléennes

comparaison, tests d'égalités, expressions arihtm => résultat : Vrai (True) ou Faux (False)

Exemple

if True  :
    print("Oups")

-----Opérateurs Booléens-----

and => conjonction, "c1 and c2" réalisée lorsque les conditions c1 et c2 sont
TOUTES LES DEUX réalisées

or => disjonction, "c1 or c2" réalisée lorsque AU MOINS UNE condition est
réalisée (parmi c1 et c2)

not => négation, "not c" réalisée lorsque la condition c N'EST PAS réalisée

----------------

exemples

if xa == xb and ya == yb:
    print("points confondus")
elif xa == xb or ya == yb:
    print("points alignés")
else:
    print("points indép")

if not 1 == 2:
    print("1 != 2")

-----------------

Priorité des opérateurs booleens

not>and>or

a or not b and c
a or ((not b) and c)

p.49 ???

if a == 0 or (b != 0 and a%b == 0):
    print("a est divisible par b")
else:
    print("a n'est pas divisible par b")

3.5 Conditionnelles imbriquées

if a <= b:
    if b < c: print("dedans")
    else: print("dehors") # b>=c
else: # a > b
    if b >= c : print("dehors")
    else: print("dedans") # b<c
"""
def molki():
    score = int(input("Entrer le score : "))
    gain = int(input("Entrer le gain : "))
    score += gain
    if score == 51:
        print("Victoire")
    else:
        if score >= 51:
            print("rip")
            score = 25
        else:
            print("Continuez")
        print(f"Nouveau score : {score}, il ne reste plus que {51-score} point(s) à gagner !")

# molki()

"""
Exercices

Ex 37
"""
def diviseurs():
    n = int(input("Entrez un nombre : "))
    for i in range(1, n+1):
        if n%i == 0:
            print(f"{i} est un diviseur de {n}")
    return

# diviseurs()

"""
Ex 38
"""
def diviseurs2():
    n = int(input("Entrez un nombre : "))
    nb_div = 2
    print(f"1 est un diviseur de {n}")
    for i in range(2, n//2 + 1):
        if n%i == 0:
            print(f"{i} est un diviseur de {n}")
            nb_div += 1
    if nb_div == 2:
        print(f"{n} est un nombre premier")
    return

# diviseurs2()
"""
Ex 39
"""
def molki2():
    n = int(input("Combien de tours : "))
    zero = 0
    for i in range(1, n+1):
        score = int(input("Entrer le score : "))
        if score == 51:
            print("Victoire")
            break
        gain = int(input("Entrer le gain : "))
        score += gain
        if zero == 3:
            print("Perdu")
            break
        elif gain == 0:
            zero += 1
            print("Aie")
            if zero == 3:
                print("Perdu")
                break
            else:
                print("Continuez")
                print(f"Nouveau score : {score}, il ne reste plus que {51-score} point(s) à gagner !")
        elif score >= 51:
            print("rip")
            score = 25
        else:
            print("Continuez")
            print(f"Nouveau score : {score}, il ne reste plus que {51-score} point(s) à gagner !")
    return


# molki2()
"""
Ex 53
"""
import math

def eq_2nd():
    a, b, c = int(input("Entrez un nombre : ")), int(input("Entrez un nombre : ")), int(input("Entrez un nombre : "))
    delta = b*b - 4*a*c
    if delta < 0:
        print("pas de solution")
    elif delta == 0:
        x = -b/2*a
        print(f"une solution : x = {x}")
    else:
        rd = math.sqrt(delta)
        x1 = (-b-rd)/2*a
        x2 = (-b+rd)/2*a
        print(f"deux solutions : x1 = {x1} et x2 = {x2}")

# eq_2nd()

"""
CHAPITRE 4
Les FONCTIONS

4.1 Problème, dessiner une face d'un dé

4.2 Définir une fonction
.
.
.

Résolution du problème
"""
def carre(x, y, n):
    up()
    goto(x, y)
    down()
    for i in range(4):
        left(90)
        forward(n)
    return


def carre_r(x, y, n):
    begin_fill()
    carre(x, y, n)
    end_fill()
    return

#carre_r(50, -50, 100)
# and the rest

"""
p.63
"""
def somme(n):
    return n*(n+1)//2

def multiples(i):
    return i*somme(999//i) # jusqu'a 1000

"""
.
.
.

Exercices

Ex 57
"""
def test_Pyth(a, b, c):
    if a*a + b*b == c*c:
        return True
    else:
        return False

# test_Pyth(3, 4, 6)
"""
ex 63
"""
def bissextile(a):
    if a % 4 == 0 and a%100 != 0:
        return True


# bissextile(2024)
"""

CHAPITRE 5
Les TABLEAUX

5.1 La Pyramide des âges
.
.
.

Exercices


