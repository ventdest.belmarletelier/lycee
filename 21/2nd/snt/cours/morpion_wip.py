plt_full = [(0, 0, "x"), (0, 1, "x"), (0, 2, "o"),
            (1, 0, "o"), (1, 1, "o"), (1, 2, "x"),
            (2, 0, "x"), (2, 1, "o"), (2, 2, "x")]

# no winner not yet full
plt2 = [(0, 0, "x"), (0, 1, "x"), (0, 2, "o"),
        (1, 0, "o"), (1, 1, "o"), (1, 2, "x"),
        (2, 0, "x"), (2, 1, "o"), (2, 2, "")]

# x win, line 0
plt3 = [(0, 0, "x"), (0, 1, "x"), (0, 2, "x"),
        (1, 0, "o"), (1, 1, "o"), (1, 2, "x"),
        (2, 0, "o"), (2, 1, "o"), (2, 2, "")]

# o win, diagonale
plt4 = [(0, 0, "o"), (0, 1, "o"), (0, 2, "x"),
        (1, 0, "o"), (1, 1, "x"), (1, 2, "x"),
        (2, 0, "x"), (2, 1, "o"), (2, 2, "")]


# o win, column 1
plt5 = [(0, 0, "x"), (0, 1, "o"), (0, 2, "x"),
        (1, 0, "o"), (1, 1, "o"), (1, 2, "x"),
        (2, 0, "o"), (2, 1, "o"), (2, 2, "")]


# x win, line 1
plt6 = [(0, 0, "o"), (0, 1, "x"), (0, 2, "x"),
        (1, 0, "x"), (1, 1, "x"), (1, 2, "x"),
        (2, 0, "o"), (2, 1, "o"), (2, 2, "")]

import ipdb

def check(plt):
    out = [p[-1] for p in plt]
    if all(out):
        print("Plateau is full, nobody won")
        return
    for piece in ('x', 'o'):
        for line in range(3):
            print(piece, line, [piece for x, y, piece in plt if x == line],
                               [f"{piece}", f"{piece}", f"{piece}"])
            if [piece for x, y, piece in plt if x == line] == [f"{piece}", f"{piece}", f"{piece}"]:
                print(f"{piece} won, line {line}")
                return
        for column in range(3):
            if [piece for x, y, piece in plt if y == column] == [f"{piece}", f"{piece}", f"{piece}"]:
                print(f"{piece} won, column {column}")
                return
        if [piece for x, y, piece in plt if x == y] == [f"{piece}", f"{piece}", f"{piece}"]:
            print(f"{piece} won, anti-diagonale")
            return
        elif [piece for x, y, piece in plt if x+y == 2] == [f"{piece}", f"{piece}", f"{piece}"]:
            print(f"{piece} won, diagonale")
            return
        else:
            print("Next player")
            return


def show(plt):
    """
    plt6 = [(0, 0, "o"), (0, 1, "x"), (0, 2, "x"),
            (1, 0, "x"), (1, 1, "x"), (1, 2, "x"),
            (2, 0, "o"), (2, 1, "o"), (2, 2, "")]

    >>> show(plt6)
    o x x
    x x x
    o o .
    """
    print(plt)


show(plt6)

# # no winner not yet full "Next player" expected
# check(plt2)
# # x win, line 0
# check(plt3)
# # o win, diagonale
# check(plt4)
# # o win, column 1
# check(plt5)
# # x win, line 1
#check(plt6)
## ===>  /!\ PLT 5 AND 6 DOESNT WORK /!\  <===
