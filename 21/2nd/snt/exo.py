l = dir(__builtins__)
print(l)

for idx, i in enumerate(l):
    print(idx, i)

ll = l[-20:]
while ll:
    line = []
    for i in range(3):
        out = ll.pop()
        line.append(out)
        if not ll:
            break
    print(line)

In [106]: ll = l[-20:]
     ...: while ll:
     ...:     line = []
     ...:     for i in range(3):
     ...:         if ll:
     ...:             line.append(ll.pop())
     ...:     print(line)



In [130]: for idx, s in enumerate(ll):
     ...:     pend = '\n' if idx % 3 == 2 else ' '
     ...:     print(s, end=pend)
     ...:
ord pow print
property range repr
reversed round set
setattr slice sorted
staticmethod str sum
super tuple type
vars zip

ll = dir(__builtins__)[-20:]
for idx, s in enumerate(ll):
    print(s, end='\n' if idx % 3 == 2 else ' ')

# ord pow print
# property range repr
# reversed round set
# setattr slice sorted
# staticmethod str sum
# super tuple type
# vars zip
