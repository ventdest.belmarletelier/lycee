# run with
# :w|!clear; python images.py

import PIL.Image as pil

img = pil.open('data/paysage.jpeg')
img.show()

print(img.size)
largeur, hauteur = img.size
img_red = pil.new('RGB', img.size)
img_sepia = pil.new('RGB', img.size)
img_green = pil.new('RGB', img.size)
img_blue = pil.new('RGB', img.size)
img_gris = pil.new('RGB', img.size)


def sepia(r, g, b):
    sepia_red = int(r*0.393 + g*0.769 + b*0.189)
    sepia_green = int(r*0.349 + g*0.686 + b*0.168)
    sepia_blue = int(r*0.272 + g*0.534 + b*0.131)
    return sepia_red, sepia_green, sepia_blue


def gris(r, g, b):
    gris = int(r*0.299 + g*0.587 + b*0.114)
    return gris, gris, gris


for w in range(largeur):
    for h in range(hauteur):
        r, g, b = img.getpixel((w, h))
        # moy = int((r+g+b)/2)
        img_red.putpixel((w, h), (r, 0, 0))
        img_sepia.putpixel((w, h), sepia(r, g, b))
        img_blue.putpixel((w, h), (0, 0, b))
        img_gris.putpixel((w, h), gris(r, g, b))
        # img_gris.putpixel((w, h), (moy, moy, moy))
        img_green.putpixel((w, h), (0, g, 0))

img_red.show()
img_green.show()
img_blue.show()
img_gris.show()
img_sepia.show()

