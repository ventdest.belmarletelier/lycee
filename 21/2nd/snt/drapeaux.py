# run with
# :w|!clear; python images.py

import PIL.Image as pil

# colors
blue = (0, 38, 86)
red = (236, 25, 32)
white = (255, 255, 255)
largeur = int(input('Rentrer la largeur du drapeau : '))
hauteur = int(largeur*2/3)
img = pil.new('RGB', (largeur, hauteur), red)
for l in range(largeur):
    for h in range(hauteur):
        if l < largeur*1/3:
            img.putpixel((l, h), blue)
        elif  largeur*1/3 <= l < largeur*2/3:
            img.putpixel((l, h), white)
        else:
            img.putpixel((l,h), red)

img.save('drapeau_francais.png', 'png')
img.show()



