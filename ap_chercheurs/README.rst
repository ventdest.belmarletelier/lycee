======
Annex
======

Sphinx installation to produce this report
=============================================

Install packages::

  $ sudo apt-get install python3-pip python3.9-venv

Create a venv::

  vent@pixel:~$ python3 -m venv ~/venv
  vent@pixel:~$ tree ~/venv/bin/
  ~/venv/bin/
  ├── activate
  ├── pip3
  ├── pip3.9
  ├── python -> python3
  ├── ...
  └── python3.9 -> python3
  vent@pixel:~$ 

Activate venv::

  vent@pixel:~$ source ~/venv/bin/activate
  (venv) vent@pixel:~$

Install Sphinx::

  (venv) vent@pixel:~/proj/lycee/ap_chercheurs$ cat requirements.txt
  sphinx
  furo
  (venv) vent@pixel:~/proj/lycee/ap_chercheurs$ pip install -r requirements.txt

Setup project with Sphinx::

(venv) vent@pixel:~/proj/lycee/ap_chercheurs$ sphinx-quickstart
(venv) vent@pixel:~/proj/lycee/ap_chercheurs$ tree
.
├── _build
├── conf.py
├── index.rst
├── Makefile
├── rapport.rst
├── README.rst
├── requirements.txt
├── _static


Change `html_theme=furo` in `conf.py`


To have laxtex A4, article with control on margins and possible 2 cols add this to
`conf.py`::

  # -- Options for LaTeX output --------------------------------
  latex_elements = {
    'papersize': 'a4paper',  # 'letterpaper' or 'a4paper'
    'pointsize': '10pt',     # font size (default is 10pt)
    'sphinxsetup': 'hmargin={1.5cm,1.5cm}, vmargin={2cm,2cm}',
    'classoptions': ',twocolumn',    # to have two columns
    }
  latex_theme = 'howto'  # 'manual' to make a book, 'howto' to make an article
  latex_logo = 'src/images/spqr_300px.png'

Crate some `rst` files::

  $ mkdir tp src
  $ touch src/antiquite.rst src/biographie.rst src/moderne.rst  

Then we need to define the main TOC table of content in the main `index.rst` file,
this TOC file will refer to rst files to include in the document::

  $ cat index.rst
  Exposé Zenobie
  ===============

  .. toctree::
     :maxdepth: 3
     :caption: Table des Matieres:

     /src/antiquite.rst
     /src/biographie.rst
     /src/moderne.rst
  $

From the folder where ``Makefile`` is,  call::

- `$ make html` to create html and point Firefox to ``_build/html/index.html``
- `$ make pdflatex` to create pdf and point evince to ``_build/latex/zenobie.pdf``

