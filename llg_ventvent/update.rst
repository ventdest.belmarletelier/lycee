Bonjour Monsieur, Madame,

Voici une rectification de la lettre résumant mon « parcours » que j’ai mis dans mon dossier « » (qui se trouve en pièce jointe),

elle concerne les examens que j’étais sensé passer durant la période suivant le confinement,

il s’agissait de l’examen HSK 4 de chinois, et de l’examen A1 de russe.

Finalement, « ... » a décidé de « supprimer » l’examen HSK 4, je n’ai donc pas pu le passer.

Quant à l’examen de russe A1, j’ai finalement décider de ne pas le passer, mais plutôt de passer le A2, en pièce jointe un papier le confirmant.

J’espère que ces éléments aiderons à « faire passer » ma candidature, sachant que « j’aimerais » faire lv2 anglais et lv3 russe.


Bonjour Madame Méar,

Je me permet de vous soliciter afin d'évaluler l'intérêt d'envoyer cette lettre
(sous forme de mail ou de Lettre en PDF) à Louis le Grand.

Le cas échéant pensez-vous qu'il faille l'envoyer à "contact@h4llg.com",  à
une autre adresse ou par courrier ?


Bonjour,

Je me permets de vous faire parvenir par ce mail, une mise à jour de la lettre
accompagnant mon dossier de candidature pour entrer dans votre établissement.

J'évoquais avoir commencé la semaine du lundi 9 mars l'apprentissage du Russe
afin de passer l'examen A1 le 27 juin. J'ai en effet passé l'examen mais, pris
d'enthousiasme pour cette langue et profitant du temps libéré par le
confinement, j'ai pu me présenter pour le niveau A2 (document en pièce jointe).

Malheureusement je n'ai pas pu me presenter à l'examen du HSK4 en juin, mon
niveau actuel correspond donc seulement au HSK3 avec une note de 261/300.

J’espère que ces éléments pourront être pris en compte favorablement afin
d'entrer en seconde Anglais en LV2 et Russe en LV3.

Cette avance modeste sur le Russe me permettra de mettre l'accent sur les
matières scientifiques correspondant à mes souhaits d'orientation futures. 

Je vous prie d’agréer, chère Madame, cher Monsieur, l’assurance de ma
considération distinguée.

Luis Belmar Letelier -- Zhou


