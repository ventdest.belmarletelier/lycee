clean:
	find . -type d -name '__pycache__' | xargs rm -fr
	make -C rapport clean
